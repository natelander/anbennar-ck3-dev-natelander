﻿# Anbennar: commenting vanilla stuff

restore_badd_fortress_scripted_effect = { } # Anbennar

destroy_the_magi_scripted_effect = { } # Anbennar

opinion_based_on_sky_burial_viewpoint_scripted_effect = {
	if = {
		limit = {
			OR = {
				faith = { has_doctrine_parameter = sky_burials_active }
				has_trait = compassionate
				has_trait = cynical
				has_trait = trusting 
				has_trait = forgiving 
				AND = {
					NOT = {	faith = { has_doctrine_parameter = sky_burials_active }	}
					ai_compassion >= medium_positive_ai_value
					ai_zeal <= medium_positive_ai_value
				}
				AND = {
					NOT = {	faith = { has_doctrine_parameter = sky_burials_active }	}
					faith = { has_doctrine = doctrine_pluralism_pluralistic }
					ai_zeal <= medium_positive_ai_value
				}
			}	
		}
		add_opinion = { 
			target = root
			opinion = 20
			modifier = fp3_loyal_to_dead_spouse_opinion
		}
	}
	else_if = {
		limit = { ai_zeal > medium_positive_ai_value }
		add_opinion = { 
			target = root
			opinion = -30
			modifier = fp3_blasphemous_practices_opinion 
		}
	}
	else = {
		add_opinion = { 
			target = root
			opinion = -10
			modifier = fp3_strange_rituals_opinion 
		}
	}
}

opinion_based_on_sky_burial_viewpoint_for_relatives_scripted_effect = {
	if = {
		limit = {
			OR = {
				faith = { has_doctrine_parameter = sky_burials_active }
				has_trait = compassionate
				has_trait = cynical
				has_trait = trusting 
				has_trait = forgiving 
				AND = {
					NOT = {	faith = { has_doctrine_parameter = sky_burials_active }	}
					ai_compassion >= medium_positive_ai_value
					ai_zeal <= medium_positive_ai_value
				}
				AND = {
					NOT = {	faith = { has_doctrine_parameter = sky_burials_active }	}
					faith = { has_doctrine = doctrine_pluralism_pluralistic }
					ai_zeal <= medium_positive_ai_value
				}
			}	
		}
		add_opinion = { 
			target = root
			opinion = 20
			modifier = fp3_benevolent_opinion
		}
	}
	else_if = {
		limit = { ai_zeal > medium_positive_ai_value }
		add_opinion = { 
			target = root
			opinion = -30
			modifier = fp3_blasphemous_practices_opinion 
		}
	}
	else = {
		add_opinion = { 
			target = root
			opinion = -10
			modifier = fp3_strange_rituals_opinion 
		}
	}
}



#####################################################################
##### SELJUK INVASION
#####################################################################

## Set up default title
give_seljuk_title_effect = { } # Anbennar

### A leader is created
spawn_seljuk_character_effect = { } # Anbennar

try_to_settle_the_seljuk_effect = { } # Anbennar

### Start war of conquest for the Seljuk heartland
start_wars_for_seljuk_empire_effect = { } # Anbennar

# focus on forming a custom persian empire and freeing turkish people first
seljuk_war_target_evaluation_and_declaration_effect = { } # Anbennar

# Fight for independence
seljuk_request_independence_from_liege_effect = { } # Anbennar

### Zanj Rebellion Effects

#This is Ali Ibn Muhammad, leader of the Zanj Rebellion
spawn_zanj_leader_character_effect = { } # Anbennar

spawn_zanj_representative_character_effect = { } # Anbennar

fp3_start_zanj_rebellion_effect = { } # Anbennar

create_zanj_courtier_effect = { } # Anbennar

create_zanj_vassal_effect = { } # Anbennar

inherit_zanj_rebellion_effect = { } # Anbennar


create_seljuk_army_effect = { } # Anbennar

create_seljuk_army_at_peace_effect = { } # Anbennar
