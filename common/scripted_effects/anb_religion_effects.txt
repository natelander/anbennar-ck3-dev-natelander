﻿mark_faiths_to_found = {
	faith:regent_court = {
		set_variable = { name = to_found }
		set_variable = { name = block_conversion_till_decision_taken }
	}
}

found_faith_no_conversion = {
	$FAITH$ = {
		remove_variable = to_found
		custom_tooltip = refound_faith_tooltip
	}
}
anb_emergent_patron_master_effect = {
	change_global_variable = {
		name = apot_count
		add = 1
	}
	switch = {
		trigger = global_var:apot_count
		1 = {
			anb_emergent_patron_effect = {
				apot_count = 1
			}
		}
		2 = {
			anb_emergent_patron_effect = {
				apot_count = 2
			}
		}
		3 = {
			anb_emergent_patron_effect = {
				apot_count = 3
			}
		}
		4 = {
			anb_emergent_patron_effect = {
				apot_count = 4
			}
		}
		5 = {
			anb_emergent_patron_effect = {
				apot_count = 5
			}
		}
		6 = {
			anb_emergent_patron_effect = {
				apot_count = 6
			}
		}
		7 = {
			anb_emergent_patron_effect = {
				apot_count = 7
			}
		}
		8 = {
			anb_emergent_patron_effect = {
				apot_count = 8
			}
		}
		9 = {
			anb_emergent_patron_effect = {
				apot_count = 9
			}
		}
		10 = {
			anb_emergent_patron_effect = {
				apot_count = 10
			}
		}
		11 = {
			anb_emergent_patron_effect = {
				apot_count = 11
			}
		}
		12 = {
			anb_emergent_patron_effect = {
				apot_count = 12
			}
		}
		13 = {
			anb_emergent_patron_effect = {
				apot_count = 13
			}
		}
		14 = {
			anb_emergent_patron_effect = {
				apot_count = 14
			}
		}
		15 = {
			anb_emergent_patron_effect = {
				apot_count = 15
			}
		}
		16 = {
			anb_emergent_patron_effect = {
				apot_count = 16
			}
		}
		17 = {
			anb_emergent_patron_effect = {
				apot_count = 17
			}
		}
		18 = {
			anb_emergent_patron_effect = {
				apot_count = 18
			}
		}
		19 = {
			anb_emergent_patron_effect = {
				apot_count = 19
			}
		}
		20 = {
			anb_emergent_patron_effect = {
				apot_count = 20
			}
		}
		21 = {
			anb_emergent_patron_effect = {
				apot_count = 21
			}
		}
		22 = {
			anb_emergent_patron_effect = {
				apot_count = 22
			}
		}
		23 = {
			anb_emergent_patron_effect = {
				apot_count = 23
			}
		}
		24 = {
			anb_emergent_patron_effect = {
				apot_count = 24
			}
		}
		25 = {
			anb_emergent_patron_effect = {
				apot_count = 25
			}
		}
		26 = {
			anb_emergent_patron_effect = {
				apot_count = 26
			}
		}
		27 = {
			anb_emergent_patron_effect = {
				apot_count = 27
			}
		}
		28 = {
			anb_emergent_patron_effect = {
				apot_count = 28
			}
		}
		29 = {
			anb_emergent_patron_effect = {
				apot_count = 29
			}
		}
		30 = {
			anb_emergent_patron_effect = {
				apot_count = 30
			}
		}
		31 = {
			anb_emergent_patron_effect = {
				apot_count = 31
			}
		}
		32 = {
			anb_emergent_patron_effect = {
				apot_count = 32
			}
		}
		33 = {
			anb_emergent_patron_effect = {
				apot_count = 33
			}
		}
		34 = {
			anb_emergent_patron_effect = {
				apot_count = 34
			}
		}
		35 = {
			anb_emergent_patron_effect = {
				apot_count = 35
			}
		}
		36 = {
			anb_emergent_patron_effect = {
				apot_count = 36
			}
		}
		37 = {
			anb_emergent_patron_effect = {
				apot_count = 37
			}
		}
		38 = {
			anb_emergent_patron_effect = {
				apot_count = 38
			}
		}
		39 = {
			anb_emergent_patron_effect = {
				apot_count = 39
			}
		}
		40 = {
			anb_emergent_patron_effect = {
				apot_count = 40
			}
		}
		41 = {
			anb_emergent_patron_effect = {
				apot_count = 41
			}
		}
		42 = {
			anb_emergent_patron_effect = {
				apot_count = 42
			}
		}
		43 = {
			anb_emergent_patron_effect = {
				apot_count = 43
			}
		}
		44 = {
			anb_emergent_patron_effect = {
				apot_count = 44
			}
		}
		45 = {
			anb_emergent_patron_effect = {
				apot_count = 45
			}
		}
		46 = {
			anb_emergent_patron_effect = {
				apot_count = 46
			}
		}
		47 = {
			anb_emergent_patron_effect = {
				apot_count = 47
			}
		}
		48 = {
			anb_emergent_patron_effect = {
				apot_count = 48
			}
		}
		49 = {
			anb_emergent_patron_effect = {
				apot_count = 49
			}
		}
		50 = {
			anb_emergent_patron_effect = {
				apot_count = 50
			}
		}
		51 = {
			anb_emergent_patron_effect = {
				apot_count = 51
			}
		}
		52 = {
			anb_emergent_patron_effect = {
				apot_count = 52
			}
		}
		53 = {
			anb_emergent_patron_effect = {
				apot_count = 53
			}
		}
		54 = {
			anb_emergent_patron_effect = {
				apot_count = 54
			}
		}
		55 = {
			anb_emergent_patron_effect = {
				apot_count = 55
			}
		}
		56 = {
			anb_emergent_patron_effect = {
				apot_count = 56
			}
		}
		57 = {
			anb_emergent_patron_effect = {
				apot_count = 57
			}
		}
		58 = {
			anb_emergent_patron_effect = {
				apot_count = 58
			}
		}
		59 = {
			anb_emergent_patron_effect = {
				apot_count = 59
			}
		}
		60 = {
			anb_emergent_patron_effect = {
				apot_count = 60
			}
		}
		61 = {
			anb_emergent_patron_effect = {
				apot_count = 61
			}
		}
		62 = {
			anb_emergent_patron_effect = {
				apot_count = 62
			}
		}
		63 = {
			anb_emergent_patron_effect = {
				apot_count = 63
			}
		}
		64 = {
			anb_emergent_patron_effect = {
				apot_count = 64
			}
		}
		65 = {
			anb_emergent_patron_effect = {
				apot_count = 65
			}
		}
		66 = {
			anb_emergent_patron_effect = {
				apot_count = 66
			}
		}
		67 = {
			anb_emergent_patron_effect = {
				apot_count = 67
			}
		}
		68 = {
			anb_emergent_patron_effect = {
				apot_count = 68
			}
		}
		69 = {
			anb_emergent_patron_effect = {
				apot_count = 69
			}
		}
		70 = {
			anb_emergent_patron_effect = {
				apot_count = 70
			}
		}
		71 = {
			anb_emergent_patron_effect = {
				apot_count = 71
			}
		}
		72 = {
			anb_emergent_patron_effect = {
				apot_count = 72
			}
		}
		73 = {
			anb_emergent_patron_effect = {
				apot_count = 73
			}
		}
		74 = {
			anb_emergent_patron_effect = {
				apot_count = 74
			}
		}
		75 = {
			anb_emergent_patron_effect = {
				apot_count = 75
			}
		}
		76 = {
			anb_emergent_patron_effect = {
				apot_count = 76
			}
		}
		77 = {
			anb_emergent_patron_effect = {
				apot_count = 77
			}
		}
		78 = {
			anb_emergent_patron_effect = {
				apot_count = 78
			}
		}
		79 = {
			anb_emergent_patron_effect = {
				apot_count = 79
			}
		}
		80 = {
			anb_emergent_patron_effect = {
				apot_count = 80
			}
		}
		81 = {
			anb_emergent_patron_effect = {
				apot_count = 81
			}
		}
		82 = {
			anb_emergent_patron_effect = {
				apot_count = 82
			}
		}
		83 = {
			anb_emergent_patron_effect = {
				apot_count = 83
			}
		}
		84 = {
			anb_emergent_patron_effect = {
				apot_count = 84
			}
		}
		85 = {
			anb_emergent_patron_effect = {
				apot_count = 85
			}
		}
		86 = {
			anb_emergent_patron_effect = {
				apot_count = 86
			}
		}
		87 = {
			anb_emergent_patron_effect = {
				apot_count = 87
			}
		}
		88 = {
			anb_emergent_patron_effect = {
				apot_count = 88
			}
		}
		89 = {
			anb_emergent_patron_effect = {
				apot_count = 89
			}
		}
		90 = {
			anb_emergent_patron_effect = {
				apot_count = 90
			}
		}
		91 = {
			anb_emergent_patron_effect = {
				apot_count = 91
			}
		}
		92 = {
			anb_emergent_patron_effect = {
				apot_count = 92
			}
		}
		93 = {
			anb_emergent_patron_effect = {
				apot_count = 93
			}
		}
		94 = {
			anb_emergent_patron_effect = {
				apot_count = 94
			}
		}
		95 = {
			anb_emergent_patron_effect = {
				apot_count = 95
			}
		}
		96 = {
			anb_emergent_patron_effect = {
				apot_count = 96
			}
		}
		97 = {
			anb_emergent_patron_effect = {
				apot_count = 97
			}
		}
		98 = {
			anb_emergent_patron_effect = {
				apot_count = 98
			}
		}
		99 = {
			anb_emergent_patron_effect = {
				apot_count = 99
			}
		}
		100 = {
			anb_emergent_patron_effect = {
				apot_count = 100
			}
		}
		101 = {
			anb_emergent_patron_effect = {
				apot_count = 101
			}
		}
		102 = {
			anb_emergent_patron_effect = {
				apot_count = 102
			}
		}
		103 = {
			anb_emergent_patron_effect = {
				apot_count = 103
			}
		}
		104 = {
			anb_emergent_patron_effect = {
				apot_count = 104
			}
		}
		105 = {
			anb_emergent_patron_effect = {
				apot_count = 105
			}
		}
		106 = {
			anb_emergent_patron_effect = {
				apot_count = 106
			}
		}
		107 = {
			anb_emergent_patron_effect = {
				apot_count = 107
			}
		}
		108 = {
			anb_emergent_patron_effect = {
				apot_count = 108
			}
		}
		109 = {
			anb_emergent_patron_effect = {
				apot_count = 109
			}
		}
		110 = {
			anb_emergent_patron_effect = {
				apot_count = 110
			}
		}
		111 = {
			anb_emergent_patron_effect = {
				apot_count = 111
			}
		}
		112 = {
			anb_emergent_patron_effect = {
				apot_count = 112
			}
		}
		113 = {
			anb_emergent_patron_effect = {
				apot_count = 113
			}
		}
		114 = {
			anb_emergent_patron_effect = {
				apot_count = 114
			}
		}
		115 = {
			anb_emergent_patron_effect = {
				apot_count = 115
			}
		}

	}
}
anb_emergent_patron_effect = {
	set_global_variable = {
		name = apotheosized_$apot_count$
		value = root
	}
	faith = {
		add_to_variable_list = { name = patron_gods target = flag:apotheosized_$apot_count$ }
	}
	random_list = {
		1 = {
			trigger = {
				has_education_martial_trigger = yes
			}
			set_global_variable = {
				name = apotheosized_$apot_count$_portfolio_category
				value = flag:anb_martial
			}
			random_list = {
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:war
					}
				}
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:chivalry
					}
				}
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:honor
					}
				}
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:strategy
					}
				}
			}
		}
		1 = {
			trigger = {
				has_education_diplomacy_trigger = yes
			}
			set_global_variable = {
				name = apotheosized_$apot_count$_portfolio_category
				value = flag:anb_diplomacy
			}
			random_list = {
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:law
					}
				}
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:order
					}
				}
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:diplomacy
					}
				}
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:judgement
					}
				}
			}
		}
		1 = {
			trigger = {
				has_education_stewardship_trigger = yes
			}
			set_global_variable = {
				name = apotheosized_$apot_count$_portfolio_category
				value = flag:anb_stewardship
			}
			random_list = {
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:craftsmanship
					}
				}
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:wealth
					}
				}
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:luck
					}
				}
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:innovation
					}
				}
			}
		}
		1 = {
			trigger = {
				has_education_intrigue_trigger = yes
			}
			set_global_variable = {
				name = apotheosized_$apot_count$_portfolio_category
				value = flag:anb_intrigue
			}
			random_list = {
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:lust
					}
				}
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:thievery
					}
				}
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:witchcraft
					}
				}
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:intrigue
					}
				}
			}
		}
		1 = {
			trigger = {
				has_education_learning_trigger = yes
			}
			set_global_variable = {
				name = apotheosized_$apot_count$_portfolio_category
				value = flag:anb_learning
			}
			random_list = {
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:inventions
					}
				}
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:scholars
					}
				}
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:wisdom
					}
				}
				1 = {
					set_global_variable = {
						name = apotheosized_$apot_count$_portfolio
						value = flag:innovations
					}
				}
			}
		}
	}
	add_to_global_variable_list = {
		name = apots
		target = flag:apotheosized_$apot_count$
	}
	#add_to_global_variable_list = {
	#	name = apots_registry
	#	target = global_var:apotheosized_$apot_count$
	#}
}
anb_remove_all_patron_modifiers = {
	remove_character_modifier = patron_the_dame_modifier
	remove_character_modifier = patron_munas_modifier
	remove_character_modifier = patron_castellos_modifier
	remove_character_modifier = patron_esmaryal_modifier
	remove_character_modifier = patron_falah_modifier
	remove_character_modifier = patron_nerat_modifier
	remove_character_modifier = patron_adean_modifier
	remove_character_modifier = patron_ryala_modifier
	remove_character_modifier = patron_balgar_modifier
	remove_character_modifier = patron_ara_modifier
	remove_character_modifier = patron_minara_modifier
	remove_character_modifier = patron_munas_modifier
	remove_character_modifier = patron_nathalyne_modifier
	remove_character_modifier = patron_begga_modifier
	remove_character_modifier = patron_uelos_modifier
	remove_character_modifier = patron_wip_modifier
	remove_character_modifier = anb_martial_patron_apot_modifier
	remove_character_modifier = anb_diplomacy_patron_apot_modifier
	remove_character_modifier = anb_stewardship_patron_apot_modifier
	remove_character_modifier = anb_intrigue_patron_apot_modifier
	remove_character_modifier = anb_learning_patron_apot_modifier
	remove_character_modifier = patron_ariadas_modifier
	remove_character_modifier = patron_histra_modifier
	remove_character_modifier = patron_wulkas_modifier
	remove_character_modifier = patron_ragna_modifier
	remove_character_modifier = patron_treyana_modifier
	remove_character_modifier = patron_hyntran_modifier
	remove_character_modifier = patron_welas_modifier
	remove_character_modifier = patron_rendan_modifier

	remove_character_modifier = patron_artanos_modifier
	remove_character_modifier = patron_merisse_modifier
	remove_character_modifier = patron_trovecos_modifier
	remove_character_modifier = patron_careslobos_modifier
	remove_character_modifier = patron_asmirethin_modifier
	remove_character_modifier = patron_damarta_modifier
	remove_character_modifier = patron_belouina_modifier
	remove_character_modifier = patron_dercanos_modifier
	remove_character_modifier = patron_turanos_modifier
	remove_character_modifier = patron_sorbodua_modifier
}
patron_set_trait = {
	patron_trait_master_effect = { EFFECT = patron_add_trait }
}
patron_trait_master_effect = {
    $EFFECT$ = { PATRON = the_dame }
    $EFFECT$ = { PATRON = munas }
	$EFFECT$ = { PATRON = castellos }
	$EFFECT$ = { PATRON = esmaryal }
	$EFFECT$ = { PATRON = falah }
	$EFFECT$ = { PATRON = nerat }
	$EFFECT$ = { PATRON = adean }
	$EFFECT$ = { PATRON = ryala }
	$EFFECT$ = { PATRON = balgar }
	$EFFECT$ = { PATRON = ara }
	$EFFECT$ = { PATRON = minara }
	$EFFECT$ = { PATRON = nathalyne }
	$EFFECT$ = { PATRON = begga }
	$EFFECT$ = { PATRON = uelos }
	$EFFECT$ = { PATRON = wip }

	$EFFECT$ = { PATRON = ariadas }
	$EFFECT$ = { PATRON = histra }
	$EFFECT$ = { PATRON = wulkas }
	$EFFECT$ = { PATRON = ragna }
	$EFFECT$ = { PATRON = treyana }
	$EFFECT$ = { PATRON = hyntran }
	$EFFECT$ = { PATRON = welas }
	$EFFECT$ = { PATRON = rendan }

	$EFFECT$ = { PATRON = artanos }
	$EFFECT$ = { PATRON = merisse }
	$EFFECT$ = { PATRON = trovecos }
	$EFFECT$ = { PATRON = careslobos }
	$EFFECT$ = { PATRON = asmirethin }
	$EFFECT$ = { PATRON = damarta }
	$EFFECT$ = { PATRON = belouina }
	$EFFECT$ = { PATRON = dercanos }
	$EFFECT$ = { PATRON = turanos }
	$EFFECT$ = { PATRON = sorbodua }

}

patron_add_trait = {
    if = {
        limit = { scope:patron = flag:$PATRON$ }
        add_character_modifier = patron_$PATRON$_modifier
    }
    else = {
		remove_character_modifier = patron_$PATRON$_modifier
	}
}