﻿morph_genes = {

	monstrous_head_attachment_01_variation = {
		group = head_neck
		inheritable = yes
		attachment_01_variation = { 
			index = 0
			male = {
				setting = { attribute = "monstrous_head_attachment01_variation_bs"	value = { min = 0.0 max = 1.0 }	 
				age = {
					mode = multiply
					curve = {
						{ 0.0 0.0 }
						{ 0.10 0.0 }
						{ 0.20 0.9 }
						{ 0.85 1.0 }
					}					
				}} 

				setting = { attribute = "monstrous_head_attachment01_young_bs"	value = { min = 0.0 max = 0.0 }	 
				age = {
					mode = add
					curve = {
						{ 0.0 1.0 }
						{ 0.10 0.0 }
						{ 1.0 0.0 }
					}					
				}} 
			}
			female = male
			boy = male
			girl = female
		}
	}	

	monstrous_head_attachment_02_variation = {
		group = head_neck
		inheritable = yes
		attachment_02_variation = { 
			index = 0
			male = {
				setting = { attribute = "monstrous_head_attachment02_variation_bs"	value = { min = 0.0 max = 1.0 }	 
				age = {
					mode = multiply
					curve = {
						{ 0.0 0.0 }
						{ 0.10 0.0 }
						{ 0.20 0.9 }
						{ 0.85 1.0 }
					}					
				}} 
			}
			female = male
			boy = male
			girl = female
		}
	}	

	monstrous_head_attachment_03_variation = {
		group = head_neck
		inheritable = yes
		attachment_03_variation = { 
			index = 0
			male = {
				setting = { attribute = "monstrous_head_attachment03_variation_bs"	value = { min = 0.0 max = 1.0 }	 
				age = {
					mode = multiply
					curve = {
						{ 0.0 0.0 }
						{ 0.10 0.0 }
						{ 0.20 0.9 }
						{ 0.85 1.0 }
					}					
				}} 
			}
			female = male
			boy = male
			girl = female
		}
	}

	monstrous_head_attachment_04_variation = {
		group = head_neck
		inheritable = yes
		attachment_04_variation = { 
			index = 0
			male = {
				setting = { attribute = "monstrous_head_attachment04_variation_bs"	value = { min = 0.0 max = 1.0 }	 
				age = {
					mode = multiply
					curve = {
						{ 0.0 0.0 }
						{ 0.10 0.0 }
						{ 0.20 0.9 }
						{ 0.85 1.0 }
					}					
				}} 
			}
			female = male
			boy = male
			girl = female
		}
	}

	eye_pupil = { #renamed from race_gene_mer_eyes
		inheritable = yes
		human_pupils = {
			index = 0
			male = {
			}
			female = male
			boy = male
			girl = male
		}

		kobold_pupils = {
			index = 1
			set_tags = "kobold_pupil"
			male = {
				decal = {
					body_part = head
					textures = {
						diffuse = "gfx/models/portraits/decals/male_head/decal_glowing_eyes_diffuse.dds"
						# normal = ""
						# properties = ""
					}
					alpha_curve = {
						{ 0.0   0.0 }
						{ 0.5   0.0 }
						{ 1.0   1.0 }
					}
					blend_modes = {#overlay/replace/hard_light/multiply
						diffuse = multiply
						# normal = replace
						# properties = replace
					}
					priority = 10
					decal_apply_order = post_skin_color # pre_skin_color | post_skin_color
				}

				decal = {
					body_part = head
					textures = {
						diffuse = "gfx/models/portraits/decals/eyes/pupil_glow_argonian.dds"
						#normal = ""
						#properties = "gfx/models/portraits/decals/eyes/full_eyes_properties_emissive.dds"
					}
					alpha_curve = {
						{ 0.0   0.0 }
						{ 0.5   0.0 }
						{ 1.0   0.65 }
					}
					blend_modes = {#overlay/replace/hard_light/multiply
						diffuse = multiply
						#normal = replace
						#properties = replace
					}
					priority = 11
					decal_apply_order = post_skin_color # pre_skin_color | post_skin_color
				}

				decal = {
					body_part = head
					textures = {
						#diffuse = "gfx/models/portraits/decals/eyes/pupil_glow_argonian.dds"
						#normal = ""
						properties = "gfx/models/portraits/decals/eyes/full_eyes_properties_emissive.dds"
					}
					alpha_curve = {
						{ 0.0   0.0 }
						{ 0.5   1.0 }
					}
					blend_modes = {#overlay/replace/hard_light/multiply
						#diffuse = multiply
						#normal = replace
						properties = replace
					}
					priority = 12
					decal_apply_order = post_skin_color # pre_skin_color | post_skin_color
				}
			}
			female = male
			boy = male
			girl = female
		}
	}

	monstrous_race_features = {
		inheritable = yes

		human_features = {
			index = 0
			male = {}
			female = male
			boy = male
			girl = male
		}

		kobold_features = {
			index = 1
			set_tags = "no_hair"
			male = {
			#setting = { attribute = "male_argonian_head_round_bs"	value = { min = 0.0 max = 1.0 }	}
				setting = { attribute = "kobold_head_bs"	value = { min = 0.0 max = 1.0 }	}
				setting = { attribute = "kobold_head_normals"	value = { min = 0.0 max = 1.0 }	}
				setting = { attribute = "kobold_head_proportions"	value = { min = 0.0 max = 1.0 }	}

				texture_override = {
					weight = 2147483647  #if there are multiple texture_overrides in a character's dna one will be picked by weighted random
					diffuse = 	"gfx/models/debug/no_diffuse.dds"
					normal = 	"gfx/models/debug/no_normal.dds"
					specular = 	"gfx/models/debug/no_properties.dds"
				}

				decal = {
					body_part = torso
					textures = {
						diffuse = 	"gfx/models/debug/no_diffuse.dds"
						normal = 	 "gfx/models/debug/no_normal.dds"
						properties = "gfx/models/debug/no_properties.dds"
					}
					alpha_curve = {              #controls decal alpha relative to gene strength. Will give a linear interpolation if left unspecified
						#gene strength%, decal alpha
						{ 0.0  1.0 }
						{ 1.0  1.0 }
					}
					blend_modes = {              #overlay/replace/hard_light/multiply
						diffuse = replace
						normal = replace
						properties = replace
					}
					priority = 0
					decal_apply_order = pre_skin_color # pre_skin_color | post_skin_color
				}

			}

			female = male
			boy = male
			girl = female
		}

		no_portrait_features = {
			index = 3
			male = {
				setting = { attribute = "bs_body_no_portrait"	value = { min = 1.0 max = 1.0 }	}
				setting = { attribute = "bs_no_portrait"	value = { min = 1.0 max = 1.0 }	}
			}

			female = male
			boy = male
			girl = female
		}
	}


	monstrous_race_decal_layer_01 = { # renamed from 	race_khajiit_fur_tortie & argonian_skin_highlights
		#inheritable = yes
		no_decal_01 = { #renamed from no_tortie
			index = 0
			male = {}
			female = male
			boy = male
			girl = male
		}

		kobold_shading_1 = { # renamed from shading_1
			index = 1
			male = {
				decal = {
					body_part = head
					textures = {
						diffuse = "gfx/models/portraits/decals/male_head/argonian/scales_01/argonians_head_shading.dds"
					}
					alpha_curve = {              #controls decal alpha relative to gene strength. Will give a linear interpolation if left unspecified
						#gene strength%, decal alpha
						{ 0.0  0.0 }
						{ 1.0  1.0 }
					}
					blend_modes = {              #overlay/replace/hard_light/multiply
						diffuse = hard_light
					}
					priority = 2
					decal_apply_order = post_skin_color # pre_skin_color | post_skin_color
				}
				decal = {
					body_part = torso
					textures = {
						diffuse = "gfx/models/portraits/decals/male_body/argonian/scales_01/argonians_body_shading.dds"
					}
					alpha_curve = {              #controls decal alpha relative to gene strength. Will give a linear interpolation if left unspecified
						#gene strength%, decal alpha
						{ 0.0  0.0 }
						{ 1.0  1.0 }
					}
					blend_modes = {              #overlay/replace/hard_light/multiply
						diffuse = hard_light
					}
					priority = 2
					decal_apply_order = post_skin_color # pre_skin_color | post_skin_color
				}
			}
			female = male
			boy = male
			girl = male
		}
	}

	monstrous_race_decal_layer_02 = { #renamed from race_khajiit_fur_shading
		group = head_neck
		inheritable = yes
		no_decal_02 = { # renamed from no_shading
			visible = no
			index = 0
			male = {}
			female = male
			boy = male
			girl = male
		}

		kobold_scales_patterns_01 = { # renamed from highlights_1
			visible = no
			index = 1
			male = {
				decal = {
					body_part = head
					textures = {
						diffuse = "gfx/models/portraits/decals/male_head/argonian/scales_01/argonians_head_highlights.dds"
					}
					alpha_curve = {              #controls decal alpha relative to gene strength. Will give a linear interpolation if left unspecified
						#gene strength%, decal alpha
						{ 0.0  0.0 }
						{ 1.0  1.0 }
					}
					blend_modes = {              #overlay/replace/hard_light/multiply
						diffuse = replace
					}
					priority = 3
					decal_apply_order = post_skin_color # pre_skin_color | post_skin_color
				}

				decal = {
					body_part = head
					textures = {
						diffuse = "gfx/models/portraits/decals/male_head/argonian/scales_01/argonians_head_shadows.dds"
						normal = "gfx/models/portraits/decals/male_head/argonian/scales_01/argonians_head_skin_normal.dds"
					}
					alpha_curve = {              #controls decal alpha relative to gene strength. Will give a linear interpolation if left unspecified
						#gene strength%, decal alpha
						{ 0.0  1.0 }
						{ 1.0  1.0 }
					}
					blend_modes = {              #overlay/replace/hard_light/multiply
						diffuse = multiply
						normal = overlay
					}
					priority = 1
					decal_apply_order = post_skin_color # pre_skin_color | post_skin_color
				}

				decal = {
					body_part = head
					textures = {
						properties = "gfx/models/portraits/decals/male_head/argonian/scales_01/argonians_head_skin_properties.dds"
					}
					alpha_curve = {              #controls decal alpha relative to gene strength. Will give a linear interpolation if left unspecified
						#gene strength%, decal alpha
						{ 0.0  0.2 }
						{ 1.0  0.2 }
					}
					blend_modes = {              #overlay/replace/hard_light/multiply
						properties = replace
					}
					priority = 2
					decal_apply_order = post_skin_color # pre_skin_color | post_skin_color
				}
				decal = {
					body_part = torso
					textures = {
						diffuse = "gfx/models/portraits/decals/male_body/argonian/scales_01/argonians_body_highlights.dds"
					}
					alpha_curve = {              #controls decal alpha relative to gene strength. Will give a linear interpolation if left unspecified
						#gene strength%, decal alpha
						{ 0.0  0.0 }
						{ 1.0  1.0 }
					}
					blend_modes = {              #overlay/replace/hard_light/multiply
						diffuse = replace
					}
					priority = 3
					decal_apply_order = post_skin_color # pre_skin_color | post_skin_color
				}

				decal = {
					body_part = torso
					textures = {
						diffuse = "gfx/models/portraits/decals/male_body/argonian/scales_01/argonians_body_shadows.dds"
						normal = "gfx/models/portraits/decals/male_body/argonian/scales_01/argonians_body_normal.dds"
					}
					alpha_curve = {              #controls decal alpha relative to gene strength. Will give a linear interpolation if left unspecified
						#gene strength%, decal alpha
						{ 0.0  1.0 }
						{ 1.0  1.0 }
					}
					blend_modes = {              #overlay/replace/hard_light/multiply
						diffuse = multiply
						normal = overlay
					}
					priority = 1
					decal_apply_order = post_skin_color # pre_skin_color | post_skin_color
				}

				decal = {
					body_part = torso
					textures = {
						properties = "gfx/models/portraits/decals/male_body/argonian/scales_01/argonians_body_properties.dds"
					}
					alpha_curve = {              #controls decal alpha relative to gene strength. Will give a linear interpolation if left unspecified
						#gene strength%, decal alpha
						{ 0.0  0.6 }
						{ 1.0  0.6 }
					}
					blend_modes = {              #overlay/replace/hard_light/multiply
						properties = replace
					}
					priority = 1
					decal_apply_order = post_skin_color # pre_skin_color | post_skin_color
				}

			}
			female = male
			boy = male
			girl = male
		}


	}



	############################################################################################################################################################
	################		     	################
	#TAIL GENES			######			##########   	########	 	###
	####						##########
	############################################################################################################################################################

	tail_length = {
		group = tail
		inheritable = yes

		tail_length = {
			index = 0
			male = {

				setting = {
					attribute = "tail_length"	
					value = { min = 0.0 max = 1.0 }
					age = {
						mode = multiply
						curve = {
							{ 0.0 0.0 }
							{ 0.2 1.0 }
						}
					}
					}
				setting = { 
					attribute = "tail_model_thick"	
					value = { min = 1.0 max = 0.0 }
					age = {
						mode = multiply
						curve = {
							{ 0.0 0.0 }
							{ 0.18 1.0 }
						}
					}	
				}
			}

			female = male
			boy = male
			girl = female
		}
	}

	tail_base_thickness = {
		group = tail
		inheritable = yes

		tail_base_thickness = {
			index = 0
			male = {

				setting = { attribute = "tail_model_base_thickness"	value = { min = 0.0 max = 1.0 }	}
			}
			female = male
			boy = male
			girl = female
		}
	}

	tail_fullness = {
		group = tail
		inheritable = yes

		tail_end_thickness = {
			index = 0
			male = {

				setting = { attribute = "tail_model_end_thickness"	value = { min = 0.0 max = 1.0 }	}
			}
			female = male
			boy = male
			girl = female

		}

		tail_end_tuft = {
			index = 1
			male = {

				setting = { attribute = "tail_model_end_tuft"	value = { min = 0.0 max = 1.0 }	}
			}

			female = male
			boy = male
			girl = female

		}
	}

	############################################################################################################################################################

	#LEGS

	############################################################################################################################################################

	leg_shape = {
		inheritable = yes

		human_legs = {
			index = 0
			male = {}
			female = male
			boy = male
			girl = female

		}
		
		kobold_digitigrade_legs = {
			index = 1
			male = {
				setting = { attribute = "kobold_digitigrade_bs"	value = { min = 1.0 max = 1.0 }	}
				setting = { attribute = "khajiit_digitigrade_clothes"	value = { min = 0.0 max = 1.0 }	}
			}
			female = male
			boy = male
			girl = female
		}
	}
}