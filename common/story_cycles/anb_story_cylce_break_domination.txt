﻿anb_story_cycle_break_domination = {

	#two vars - willpower (victim) and grasp (controller)
	on_setup = {
	
		set_variable = {
			name = willpower_variable
			value = 0
		}
		
		#factor in traits
		
		set_variable = {
			name = grasp_variable
			value = 10
		}
		
		#factor in traits and learning
	}
	
	on_end = {
		if = {	# Controller died
			limit = { story_owner.var:controller = { is_alive = no } }
			story_owner = { trigger_event = anb_dominate_victim.5 }
		}
		else_if = {	# Succeded
			limit = { story_owner.var:controller = { is_alive = yes } }
			story_owner.var:controller = {
				remove_hook = {
					target = story_owner
					type = dominate_hook
				}
				trigger_event = anb_dominate_victim.2
			}
			story_owner = { trigger_event = anb_dominate_victim.4 }
		}
		# Remove Var in all 3 cases
		remove_variable = controller
	}
	
	on_owner_death = {
		scope:story = { end_story = yes }
	}
	
	#First Phase: Restore Order
	effect_group = {
		months = { 1 3 }
		
		trigger = { story_owner.var:willpower_variable < 5 }
		
		triggered_effect = {
			trigger = {
				always = yes
			}
		}
	}
	
	#Second Phase: Build Mental Defenses
	effect_group = {
		months = { 3 5 }
		
		trigger = { story_owner.var:willpower_variable > 4 }
		
		triggered_effect = {
			trigger = {
				always = yes
			}
		}
	}
	
	#Third Phase: Drive Them Out - Possibility to gain some willpower and try it (duel) or lose some and don't
	effect_group = {
		days = 1
		
		trigger = { story_owner.var:willpower_variable >= story_owner.var:grasp_variable }
		
		triggered_effect = {
			trigger = {
				always = yes
			}
		}
	}
	
	#Grasp events/responses??
}