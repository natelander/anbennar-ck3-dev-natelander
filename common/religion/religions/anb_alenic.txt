﻿alenic_religion = {
	family = rf_precastanorian
	graphical_faith = pagan_gfx
	doctrine = precastanorian_hostility_doctrine 
	
	pagan_roots = yes

	#Main Group
	doctrine = doctrine_no_head	
	doctrine = doctrine_gender_male_dominated	
	doctrine = doctrine_pluralism_righteous
	doctrine = doctrine_theocracy_temporal

	#Marriage
	doctrine = doctrine_monogamy
	doctrine = doctrine_divorce_approval
	doctrine = doctrine_bastardry_legitimization
	doctrine = doctrine_consanguinity_cousins

	#Crimes
	doctrine = doctrine_homosexuality_shunned
	doctrine = doctrine_adultery_men_shunned
	doctrine = doctrine_adultery_women_shunned
	doctrine = doctrine_kinslaying_shunned
	doctrine = doctrine_deviancy_shunned
	doctrine = doctrine_witchcraft_shunned
	
	#Clerical Functions
	doctrine = doctrine_clerical_function_recruitment
	doctrine = doctrine_clerical_gender_male_only
	doctrine = doctrine_clerical_marriage_allowed
	doctrine = doctrine_clerical_succession_temporal_fixed_appointment
	
	#Allow pilgrimages
	doctrine = doctrine_pilgrimage_encouraged
	
	#Funeral tradition
	doctrine = doctrine_funeral_stoic
	
	traits = {	
		virtues = {
			diligent
			brave
			honest
			lifestyle_reveler
			lifestyle_hunter
		}
		sins = {
			lazy
			craven
			deceitful
			shy
		}
	}

	custom_faith_icons = {	#TODO
		custom_faith_1 custom_faith_2 custom_faith_3 custom_faith_4 custom_faith_5 custom_faith_6 custom_faith_7 custom_faith_8 custom_faith_9 custom_faith_10 dualism_custom_1 zoroastrian_custom_1 zoroastrian_custom_2 buddhism_custom_1 buddhism_custom_2 buddhism_custom_3 buddhism_custom_4 taoism_custom_1 yazidi_custom_1 sunni_custom_2 sunni_custom_3 sunni_custom_4 ibadi_custom muhakkima_1 muhakkima_2 muhakkima_4 muhakkima_5 muhakkima_6 judaism_custom_1
	}

	holy_order_names = { #TODO
		{ name = "holy_order_riders_of_artanos" }
		{ name = "holy_order_smiths_of_asmirethin" }
		{ name = "holy_order_dioue_guardians" }
	}

	holy_order_maa = { armored_footmen }	#todo

	localization = {
		#HighGod - Ariadas
		HighGodName = alenic_high_god_name
		HighGodName2 = alenic_high_god_name
		HighGodNamePossessive = alenic_high_god_name_possessive
		HighGodNameSheHe = CHARACTER_SHEHE_HE
		HighGodHerselfHimself = CHARACTER_HIMSELF
		HighGodHerHis = CHARACTER_HERHIS_HIS
		HighGodNameAlternate = alenic_high_god_name_alternate #the Lord of Storms
		HighGodNameAlternatePossessive = alenic_high_god_name_alternate_possessive

		#Creator - Ariadas
		CreatorName = alenic_high_god_name
		CreatorNamePossessive = alenic_high_god_name_possessive
		CreatorSheHe = CHARACTER_SHEHE_HE
		CreatorHerHis = CHARACTER_HERHIS_HIS
		CreatorHerHim = CHARACTER_HERHIM_HIM

		#HealthGod - Treyana
		HealthGodName = alenic_health_god_name
		HealthGodNamePossessive = alenic_health_god_name_possessive
		HealthGodSheHe = CHARACTER_SHEHE_SHE
		HealthGodHerHis = CHARACTER_HERHIS_HER
		HealthGodHerHim = CHARACTER_HERHIM_HER
		
		#FertilityGod - Histra
		FertilityGodName = alenic_fertility_god_name
		FertilityGodNamePossessive = alenic_fertility_god_name_possessive
		FertilityGodSheHe = CHARACTER_SHEHE_SHE
		FertilityGodHerHis = CHARACTER_HERHIS_HER
		FertilityGodHerHim = CHARACTER_HERHIM_HER

		#WealthGod - Histra
		WealthGodName = alenic_fertility_god_name
		WealthGodNamePossessive = alenic_fertility_god_name_possessive
		WealthGodSheHe = CHARACTER_SHEHE_SHE
		WealthGodHerHis = CHARACTER_HERHIS_HER
		WealthGodHerHim = CHARACTER_HERHIM_HER

		#HouseholdGod - Histra
		HouseholdGodName = alenic_fertility_god_name
		HouseholdGodNamePossessive = alenic_fertility_god_name_possessive
		HouseholdGodSheHe = CHARACTER_SHEHE_SHE
		HouseholdGodHerHis = CHARACTER_HERHIS_HER
		HouseholdGodHerHim = CHARACTER_HERHIM_HER

		#FateGod - Hyntran
		FateGodName = alenic_fate_god_name
		FateGodNamePossessive = alenic_fate_god_name_possessive
		FateGodSheHe = CHARACTER_SHEHE_HE
		FateGodHerHis = CHARACTER_HERHIS_HIS
		FateGodHerHim = CHARACTER_HERHIM_HIM

		#KnowledgeGod - Welas
		KnowledgeGodName = alenic_knowledge_god_name
		KnowledgeGodNamePossessive = alenic_knowledge_god_name_possessive
		KnowledgeGodSheHe = CHARACTER_SHEHE_HE
		KnowledgeGodHerHis = CHARACTER_HERHIS_HIS
		KnowledgeGodHerHim = CHARACTER_HERHIM_HIM

		#WarGod - Wulkas
		WarGodName = alenic_war_god_name
		WarGodNamePossessive = alenic_war_god_name_possessive
		WarGodSheHe = CHARACTER_SHEHE_HE
		WarGodHerHis = CHARACTER_HERHIS_HIS
		WarGodHerHim = CHARACTER_HERHIM_HIM

		#TricksterGod - Rendan
		TricksterGodName = alenic_trickster_god_name
		TricksterGodNamePossessive = alenic_trickster_god_name_possessive
		TricksterGodSheHe = CHARACTER_SHEHE_HE
		TricksterGodHerHis = CHARACTER_HERHIS_HIS
		TricksterGodHerHim = CHARACTER_HERHIM_HIM

		#NightGod - Duathra
		NightGodName = alenic_night_god_name
		NightGodNamePossessive = alenic_night_god_name_possessive
		NightGodSheHe = CHARACTER_SHEHE_HE
		NightGodHerHis = CHARACTER_HERHIS_HIS
		NightGodHerHim = CHARACTER_HERHIM_HIM

		#WaterGod - Welas
		WaterGodName = alenic_knowledge_god_name
		WaterGodNamePossessive = alenic_knowledge_god_name_possessive
		WaterGodSheHe = CHARACTER_SHEHE_HE
		WaterGodHerHis = CHARACTER_HERHIS_HIS
		WaterGodHerHim = CHARACTER_HERHIM_HIM


		PantheonTerm = religion_the_gods
		PantheonTerm2 = religion_the_gods_2
		PantheonTerm3 = religion_the_gods_3
		PantheonTermHasHave = pantheon_term_have
		GoodGodNames = {
			alenic_high_god_name
			alenic_high_god_name_alternate
			alenic_health_god_name
			alenic_fertility_god_name
			alenic_fate_god_name
			alenic_knowledge_god_name
			alenic_war_god_name
			deity_ragna
		}
		
		
		DevilName = alenic_devil_name #The World Serpent
		DevilNamePossessive = alenic_devil_name_possessive
		DevilSheHe = CHARACTER_SHEHE_HE
		DevilHerHis = CHARACTER_HERHIS_HIS
		DevilHerselfHimself = alenic_devil_herselfhimself
		EvilGodNames = {
			alenic_devil_name
			alenic_trickster_god_name
			alenic_night_god_name
		}
		HouseOfWorship = cannorian_pantheon_house_of_worship #temple
		HouseOfWorship2 = cannorian_pantheon_house_of_worship
		HouseOfWorship3 = cannorian_pantheon_house_of_worship
		HouseOfWorshipPlural = cannorian_pantheon_house_of_worship_plural
		ReligiousSymbol = alenic_religious_symbol #eagle
		ReligiousSymbol2 = alenic_religious_symbol
		ReligiousSymbol3 = alenic_religious_symbol
		ReligiousText = alenic_religious_text #the ancient legends
		ReligiousText2 = alenic_religious_text
		ReligiousText3 = alenic_religious_text
		ReligiousHeadName = cannorian_pantheon_religious_head_title #High Priest
		ReligiousHeadTitleName = cannorian_pantheon_religious_head_title_name #High Priesthood
		DevoteeMale = cannorian_pantheon_devotee_male #cleric
		DevoteeMalePlural = cannorian_pantheon_devotee_male_plural
		DevoteeFemale = cannorian_pantheon_devotee_female #cleric
		DevoteeFemalePlural = cannorian_pantheon_devotee_female_plural
		DevoteeNeuter = cannorian_pantheon_devotee_neuter #cleric
		DevoteeNeuterPlural = cannorian_pantheon_devotee_neuter_plural
		PriestMale = cannorian_pantheon_priest #priest
		PriestMalePlural = cannorian_pantheon_priest_plural
		PriestFemale = cannorian_pantheon_priest_female #priestess
		PriestFemalePlural = cannorian_pantheon_priest_female_plural
		PriestNeuter = cannorian_pantheon_priest #priest
		PriestNeuterPlural = cannorian_pantheon_priest_plural
		AltPriestTermPlural = cannorian_pantheon_priest_term_plural #Priests
		BishopMale = cannorian_pantheon_bishop #high priest
		BishopMalePlural = cannorian_pantheon_bishop_plural
		BishopFemale = cannorian_pantheon_bishop_female #high priestess
		BishopFemalePlural = cannorian_pantheon_bishop_female_plural
		BishopNeuter = cannorian_pantheon_bishop #high_priest
		BishopNeuterPlural = cannorian_pantheon_bishop_plural
		DivineRealm = alenic_divine_realm #Halls of Ariadas
		DivineRealm2 = alenic_divine_realm
		DivineRealm3 = alenic_divine_realm
		PositiveAfterLife = alenic_divine_realm #Halls of Ariadas
		PositiveAfterLife2 = alenic_divine_realm
		PositiveAfterLife3 = alenic_divine_realm
		NegativeAfterLife = alenic_afterlife #Halls of Duathra
		NegativeAfterLife2 = alenic_afterlife
		NegativeAfterLife3 = alenic_afterlife
		DeathDeityName = alenic_death_name #Duathra
		DeathDeityNamePossessive = alenic_death_name_possessive
		DeathDeitySheHe = CHARACTER_SHEHE_SHE
		DeathDeityHerHis = CHARACTER_HERHIS_HER
		DeathDeityHerHim = CHARACTER_HERHIM_HER

		GHWName = ghw_purification
		GHWNamePlural = ghw_purifications
	}

	faiths = {
		ansuwir = {
			color = { 81 42 125 }
			icon = the_dame
			reformed_icon = the_dame
			
			holy_site = vertesk
			holy_site = aldtempel
			holy_site = steelhyl
			holy_site = houndsmoor
			holy_site = cannwic
			
			doctrine = tenet_bhakti
			doctrine = tenet_warmonger
			doctrine = tenet_freolriaz
			
			doctrine = unreformed_faith_doctrine
		}
		
		moors_cult = {
			color = { 199 191 228 }
			icon = the_dame
			reformed_icon = the_dame
			
			holy_site = wightsgate
			holy_site = eyegard
			holy_site = wispmire
			holy_site = houndsmoor
			holy_site = vertesk
			
			doctrine = tenet_human_sacrifice
			doctrine = tenet_communal_identity
			doctrine = tenet_freolriaz
			
			doctrine = unreformed_faith_doctrine
			
			#Main Group
			doctrine = doctrine_pluralism_pluralistic
			doctrine = doctrine_theocracy_lay_clergy

			#Crimes
			doctrine = doctrine_adultery_women_crime
			doctrine = doctrine_kinslaying_any_dynasty_member_crime
			doctrine = doctrine_witchcraft_crime
			
			#Clerical Functions
			doctrine = doctrine_clerical_succession_temporal_appointment
			
			#Allow pilgrimages
			doctrine = doctrine_pilgrimage_mandatory
			
			#Funeral tradition
			doctrine = doctrine_funeral_cremation
			
			localization = {
				#HighGod - Moarstriaz
				HighGodName = moors_cult_high_god_name
				HighGodName2 = moors_cult_high_god_name
				HighGodNamePossessive = moors_cult_high_god_name_possessive
				HighGodNameAlternate = moors_cult_high_god_name_alternate #the Fiery One
				HighGodNameAlternatePossessive = moors_cult_high_god_name_alternate_possessive
			}
		}
	}
}
		