#k_hasr
##d_hasr
###c_hasr
640 = {		#Hasr

    # Misc
    culture = surani
    religion = rite_of_hammura
	holding = castle_holding

    # History
}

6644 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6645 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6646 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6647 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

###c_dazar_suran
641 = {		#Dazar Suran

    # Misc
    culture = surani
    religion = rite_of_hammura
	holding = castle_holding

    # History
}

6648 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6649 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6650 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

###c_asranaz
638 = {		#Asranaz

    # Misc
    culture = surani
    religion = rite_of_hammura
	holding = castle_holding

    # History
}

6642 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6643 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

###c_zankumar
639 = {		#Zankumar

    # Misc
    culture = surani
    religion = rite_of_hammura
	holding = castle_holding

    # History
}

6639 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6640 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6641 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

##d_panuses
###c_panubar
635 = {		#Panubar

    # Misc
    culture = surani
    religion = rite_of_hammura
	holding = castle_holding

    # History
}

6629 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6630 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6631 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

###c_utlazna
637 = {		#Utlazna

    # Misc
    culture = surani
    religion = rite_of_hammura
	holding = castle_holding

    # History
}

6634 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6635 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

###c_durklum
636 = {		#Durklum

    # Misc
    culture = surani
    religion = rite_of_hammura
	holding = castle_holding

    # History
}

6632 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6633 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

##d_eduz_vacyn
631 = {		#Eduz-Vacyn

    # Misc
    culture = surani
    religion = rite_of_hammura
	holding = church_holding

    # History
}

6625 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6626 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6627 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6628 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

###c_kufu
634 = {		#Kufu

    # Misc
    culture = surani
    religion = rite_of_hammura
	holding = castle_holding

    # History
}

6636 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6637 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}

6638 = {		#

    # Misc
    culture = surani
    religion = rite_of_hammura
    holding = none
    # History
}