#k_sareyand
##d_sareyand
###c_sareyand
625 = {		#Sareyand

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = castle_holding

    # History
}

6601 = {		#

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = none
    # History
}

6602 = {		#

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = none
    # History
}

6603 = {		#

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = none
    # History
}

6604 = {		#

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = none
    # History
}

###c_surzan
626 = {		#Surzan

    # Misc
    culture = zanite
    religion = rite_of_hammura
	holding = castle_holding

    # History
}

6605 = {		#

    # Misc
    culture = zanite
    religion = rite_of_hammura
	holding = none
    # History
}

6606 = {		#

    # Misc
    culture = zanite
    religion = rite_of_hammura
	holding = none
    # History
}

6607 = {		#

    # Misc
    culture = zanite
    religion = rite_of_hammura
	holding = none
    # History
}

###c_duklum
627 = {		#Duklum

    # Misc
    culture = zanite
    religion = rite_of_hammura
	holding = castle_holding

    # History
}

6608 = {		#

    # Misc
    culture = zanite
    religion = rite_of_hammura
	holding = none
    # History
}

6609 = {		#

    # Misc
    culture = zanite
    religion = rite_of_hammura
	holding = none
    # History
}

6610 = {		#

    # Misc
    culture = zanite
    religion = rite_of_hammura
	holding = none
    # History
}

##d_zanbar
###c_zanbar
604 = {		#Zanbar

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = castle_holding

    # History
}

6581 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}

6582 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}

6583 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}

6584 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}

###c_kuzkumar
611 = {		#Kuzkumar

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = castle_holding

    # History
}

6585 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}

6586 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}

##d_harklum
###c_harklum
607 = {		#Harklum

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = castle_holding

    # History
}

6587 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}

6588 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}

6589 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}

6590 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}

###c_traz_suran
612 = {		#Traz Suran

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = castle_holding

    # History
}

6595 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}

6596 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}

6597 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}

6598 = {		#

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none
    # History
}