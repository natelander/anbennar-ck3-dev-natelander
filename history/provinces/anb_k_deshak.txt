381 = { #Krodamat

    # Misc
    culture = deshaki
    religion = mother_akasik
	holding = castle_holding

    # History
}

382 = { #Deshak

    # Misc
    culture = deshaki
    religion = mother_akasik
	holding = castle_holding

    # History
}

397 = { #Hapak

    # Misc
    culture = stonewing
    religion = mother_akasik
	holding = castle_holding

    # History
}

399 = { #Unrawu

    # Misc
    culture = deshaki
    religion = father_aiji
	holding = castle_holding

    # History
}

403 = { #Rhamsopot

    # Misc
    culture = deshaki
    religion = mother_akasik
	holding = castle_holding

    # History
}

404 = { #Mushesh

    # Misc
    culture = deshaki
    religion = father_amahash
	holding = castle_holding

    # History
}

405 = { #Khalur

    # Misc
    culture = deshaki
    religion = mother_akasik
	holding = castle_holding

    # History
}

453 = { #Orispot

    # Misc
    culture = deshaki
    religion = mother_akhat
	holding = castle_holding

    # History
}

458 = { #Orisat

    # Misc
    culture = deshaki
    religion = father_aiji
	holding = castle_holding

    # History
}