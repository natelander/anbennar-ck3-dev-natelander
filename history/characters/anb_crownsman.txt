﻿
crownsman0001 = { # Teagan of Teagansfield
	name = "Teagan"
	dynasty = dynasty_teagansfield
	religion = cult_of_ara
	culture = crownsman
	
	trait = race_human
	trait = education_stewardship_1
	trait = brave
	trait = compassionate
	trait = just
	trait = peasant_leader
	trait = organizer
	
	977.12.5 = {
		birth = yes
	}
	
	995.3.19 = {
		set_relation_lover = character:crownsman0002
		add_spouse = crownsman0002
	}
}

crownsman0002 = { # Wife of Teagan, Isabel
	name = "Isabel"
	# lowborn
	religion = cult_of_ara
	culture = crownsman
	female = yes
	
	trait = race_human
	trait = education_diplomacy_2
	trait = diligent
	trait = generous
	trait = patient
	trait = beauty_good_1
	
	977.11.18 = {
		birth = yes
	}
	
	995.3.19 = {
		add_spouse = crownsman0001
	}
}

crownsman0003 = { # Son of Teagan, Lorran of Teagansfield
	name = "Lorran"
	dynasty = dynasty_teagansfield
	religion = cult_of_ara
	culture = crownsman
	
	trait = race_human
	trait = education_martial_2
	trait = stubborn
	trait = gregarious
	trait = just
	trait = lifestyle_hunter
	trait = forest_fighter
	
	father = crownsman0001
	mother = crownsman0002
	
	994.7.30 = {
		birth = yes
	}
	
	1013.1.8 = {
		set_relation_lover = character:crownsman0006
		add_spouse = crownsman0006
		
		add_trait_xp = {
			trait = lifestyle_hunter
			track = hunter
			value = 50
		}
	}
}

crownsman0004 = { # Daughter of Teagan, Marina of Teagansfield
	name = "Marina"
	dynasty = dynasty_teagansfield
	religion = cult_of_ara
	culture = crownsman
	female = yes
	
	trait = race_human
	trait = education_diplomacy_1
	trait = lazy
	trait = content
	trait = honest
	trait = beauty_good_1
	
	father = crownsman0001
	mother = crownsman0002
	
	997.6.8 = {
		birth = yes
	}
}

crownsman0005 = { # Daughter of Teagan, Amina of Teagansfield
	name = "Amina"
	dynasty = dynasty_teagansfield
	religion = cult_of_ara
	culture = crownsman
	female = yes
	
	trait = race_human
	trait = education_learning_3
	trait = chaste
	trait = calm
	trait = cynical
	trait = beauty_good_1
	
	father = crownsman0001
	mother = crownsman0002
	
	1004.11.8 = {
		birth = yes
		set_sexuality = asexual
	}
}

crownsman0006 = { #Wife of Lorran of Teagansfield
	name = "GisE_le"
	# lowborn
	religion = adenican_adean
	culture = adeanic
	female = yes
	
	trait = race_human
	trait = education_learning_1
	trait = temperate
	trait = humble
	trait = trusting
	
	996.5.16 = {
		birth = yes
	}
	
	1013.1.8 = {
		add_spouse = crownsman0003
	}
}

crownsman0007 = {
	name = "Teagan"
	dynasty = dynasty_teagansfield
	religion = cult_of_ara
	culture = crownsman
	
	trait = race_human
	trait = bossy
	
	father = crownsman0003
	mother = crownsman0006
	
	1017.3.20 = {
		birth = yes
	}
}
