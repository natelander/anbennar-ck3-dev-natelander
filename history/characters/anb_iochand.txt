﻿iochand_0001 = { #King Carwick of Iochand
	name = "Carwick"
	dna = 47_carwick_iochand
	dynasty = dynasty_iochand #Iochand
	religion = "iochand_cult"
	culture = "creek_gnomish"
	
	trait = race_gnome
	trait = education_diplomacy_3
	trait = gregarious
	trait = impatient
	trait = lustful
	trait = shrewd

	mother = nimsnoms_0001
	father = iochand_0101
	
	861.3.2 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}

	922.5.2 = {
		add_spouse = nimsnoms_0002
	}
}

iochand_0002 = { #Princess Tripp of Iochand
	name = "Tripp"
	dna = tripp_iochand
	dynasty = dynasty_iochand #Iochand
	religion = "iochand_cult"
	sexuality = homosexual
	culture = "creek_gnomish"
	female = yes
	
	trait = race_gnome
	trait = education_stewardship_4
	trait = patient
	trait = diligent
	trait = temperate
	
	mother = nimsnoms_0002
	father = iochand_0001
	
	999.10.8 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}

	1021.1.1 = {
		effect = {
			set_relation_lover = character:southroy_0002
		}
	}
}

iochand_0003 = { #Princess Emy of Iochand
	name = "Emy"
	dynasty = dynasty_iochand #Iochand
	religion = "iochand_cult"
	culture = "creek_gnomish"
	female = yes
	
	trait = race_gnomeling
	trait = education_learning_3
	trait = content
	trait = trusting
	trait = shy
	
	father = iochand_0001
	
	1004.6.9 = {
		birth = yes
		remove_trait = race_gnome
	}
}

iochand_0004 = { #Bindle of Iochand
	name = "Bindle"
	dynasty = dynasty_iochand #Iochand
	religion = "iochand_cult"
	culture = "creek_gnomish"
	female = yes
	
	trait = race_gnome
	trait = education_stewardship_1
	trait = arrogant
	trait = greedy
	trait = gluttonous

	mother = nimsnoms_0001
	father = iochand_0101
	
	960.6.6 = {
		birth = yes
	}

	922.5.2 = {
		add_spouse = nimsnoms_0003
	}
}

iochand_0005 = { #Fizwick 'the fool' of Iochand
	name = "Fizwick"
	dynasty = dynasty_iochand #Iochand
	religion = "iochand_cult"
	culture = "creek_gnomish"
	
	trait = race_gnome
	trait = intellect_bad_2

	mother = iochand_0004
	father = nimsnoms_0003
	
	1021.11.11 = {
		birth = yes
	}
}

iochand_0101 = { #last King of Iochand, Schleemo
	name = "Schleemo"
	dynasty = dynasty_iochand #Iochand
	religion = "iochand_cult"
	culture = "creek_gnomish"
	
	trait = race_gnome
	trait = education_martial_prowess_2

	808.2.1 = {
		birth = yes
	}

	850.6.7 = {
		add_spouse = nimsnoms_0001
	}

	962.11.13 = {
		death = {
			death_reason = death_ill
		}
	}
}

iochand_0102 = { #First King of Iochand, Fizwick Allspark
	name = "Fizwick"
	dynasty = dynasty_iochand #Iochand
	religion = "iochand_cult"
	culture = "creek_gnomish"
	
	trait = race_gnome
	trait = education_stewardship_3
	
	360.9.6 = {
		birth = yes
	}

	552.12.3 = {
		death = {
			death_reason = death_natural_causes
		}
	}
}

southroy_0001 = { #Mindi II of Southroy
	name = "Mindi"
	dna = mindi_southroy
	dynasty = dynasty_southroy #Southroy
	religion = "iochand_cult"
	culture = "creek_gnomish"
	female = yes
	
	trait = race_gnome
	trait = education_diplomacy_2
	trait = content
	trait = paranoid
	trait = diligent
	
	mother = nimsnoms_0004
	father = southroy_0101

	961.7.19 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}

	1003.5.2 = {
		add_spouse = nimsnoms_0006
	}

	1022.1.1 = {
		effect = {
			set_relation_rival = character:timekeeper_0001
		}
	}

}

southroy_0002 = { #Poppy of Southroy
	name = "Poppy"
	dynasty = dynasty_southroy #Southroy
	religion = "iochand_cult"
	sexuality = homosexual
	culture = "creek_gnomish"
	female = yes
	
	trait = race_gnome
	trait = education_intrigue_2
	trait = content
	trait = patient
	trait = compassionate

	mother = nimsnoms_0004
	father = southroy_0101
	
	983.1.4 = {
		birth = yes
	}
}

southroy_0003 = { #Gibbles of Southroy
	name = "Gibbles"
	dynasty = dynasty_southroy #Southroy
	religion = "iochand_cult"
	culture = "creek_gnomish"
	
	trait = race_gnome
	trait = content
	trait = humble
	trait = charming

	mother = southroy_0001
	father = nimsnoms_0006
	
	1007.8.17 = {
		birth = yes
	}
}

southroy_0101 = { #Last Duke of Southroy, Torwick
	name = "Torwick"
	dynasty = dynasty_southroy #Southroy
	religion = "iochand_cult"
	culture = "creek_gnomish"
	
	trait = race_gnome
	trait = education_martial_3
	trait = content
	trait = brave
	
	854.8.17 = {
		birth = yes
	}

	916.1.12 = {
		add_spouse = nimsnoms_0004
	}

	996.4.23 = {
		death = {
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

southroy_0102 = { #First Duchess of Southroy, Erna Tonks
	name = "Erna"
	dynasty = dynasty_southroy #Southroy
	religion = "iochand_cult"
	culture = "creek_gnomish"
	female = yes
	
	trait = race_gnome
	trait = education_diplomacy_3
	
	327.1.7 = {
		birth = yes
	}

	516.2.13 = {
		death = {
			death_reason = death_natural_causes
		}
	}
}

nimsnoms_0001 = { #Queen-mother Ara of Iochand
	name = "Ara"
	dynasty = dynasty_nimsnoms #Nimsnoms
	religion = "iochand_cult"
	culture = "creek_gnomish"
	female = yes
	
	trait = race_gnome
	trait = education_learning_2
	trait = shy
	trait = forgiving
	trait = honest

	
	837.5.14 = {
		birth = yes
	}
}

nimsnoms_0002 = { #Queen Tilly of Iochand
	name = "Tilly"
	dynasty = dynasty_nimsnoms #Nimsnoms
	religion = "iochand_cult"
	culture = "creek_gnomish"
	female = yes
	
	trait = race_gnome
	trait = education_intrigue_2
	trait = impatient
	trait = stubborn
	trait = ambitious

	
	935.5.11 = {
		birth = yes
	}
}

nimsnoms_0003 = { #Bort of Iochand
	name = "Bort"
	dynasty = dynasty_nimsnoms #Nimsnoms
	religion = "iochand_cult"
	culture = "creek_gnomish"
	
	trait = race_gnome
	trait = education_martial_3
	trait = brave
	trait = wrathful
	trait = callous
	trait = one_legged

	
	949.3.30 = {
		birth = yes
	}
}

nimsnoms_0004 = { #Cinda, mother of Duchess Mindi & Poppy
	name = "Cinda"
	dynasty = dynasty_nimsnoms #Nimsnoms
	religion = "iochand_cult"
	culture = "creek_gnomish"
	female = yes
	
	trait = race_gnome
	trait = education_learning_2
	trait = arbitrary
	trait = calm
	trait = diligent

	
	822.1.29 = {
		birth = yes
	}
}

nimsnoms_0005 = { #Jomble, Jester of Iochand
	name = "Jomble"
	dynasty = dynasty_nimsnoms #Nimsnoms
	religion = "iochand_cult"
	culture = "creek_gnomish"
	
	trait = race_gnome
	trait = education_learning_2
	trait = deceitful
	trait = temperate
	trait = fickle
	trait = disfigured

	
	935.2.29 = {
		birth = yes
	}

	1021.1.1 = {
		employer = iochand_0001
		give_council_position = councillor_spymaster
	}
}

nimsnoms_0006 = { #Duke Melbin
	name = "Melbin"
	dynasty = dynasty_nimsnoms #Nimsnoms
	religion = "iochand_cult"
	culture = "creek_gnomish"
	
	trait = race_gnome
	trait = education_learning_2
	trait = gregarious
	trait = generous
	trait = fickle
	trait = beauty_good_1
	trait = lifestyle_reveler

	922.1.21 = {
		birth = yes
	}
}

nimsnoms_0007 = { #Magdalene of Haysfield
	name = "Magdalene"
	dynasty = dynasty_nimsnoms #
	religion = "iochand_cult"
	culture = "iochander"
	female = yes
	
	trait = race_human
	trait = education_diplomacy_2
	trait = ambitious
	trait = gregarious
	trait = gluttonous

	982.4.16 = {
		birth = yes
	}
}

timekeeper_0001 = { #Royan of Portnamm
	name = "Royan"
	dna = royan_timekeeper
	dynasty = dynasty_timekeeper #
	religion = "iochand_cult"
	culture = "creek_gnomish"
	
	trait = race_gnome
	trait = education_diplomacy_3
	trait = ambitious
	trait = stubborn
	trait = callous

	
	873.10.18 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
}

donnderward_0001 = { #Count Cecill of Thunderward
	name = "Cecill"
	dynasty = dynasty_donnderward #
	religion = "iochand_cult"
	culture = "iochander"
	
	trait = race_human
	trait = education_stewardship_2
	trait = diligent
	trait = vengeful
	trait = patient

	father = donnderward_0101
	
	974.12.3 = {
		birth = yes
	}

	992.1.1 = {
		add_spouse = rewantis_0027
	}
}

donnderward_0002 = { #Roen of Thunderward
	name = "Roen"
	dynasty = dynasty_donnderward #
	religion = "iochand_cult"
	culture = "iochander"
	
	trait = race_human
	trait = education_martial_2
	trait = brave
	trait = forgiving
	trait = patient

	father = donnderward_0001
	mother = rewantis_0027

	998.11.7 = {
		birth = yes
	}
}

donnderward_0003 = { #Poppy of Thunderward
	name = "Poppy"
	dynasty = dynasty_donnderward #
	religion = "iochand_cult"
	culture = "iochander"
	female = yes
	
	trait = race_human
	trait = education_learning_1
	trait = lazy
	trait = impatient
	trait = compassionate

	father = donnderward_0001
	mother = rewantis_0027

	1003.5.22 = {
		birth = yes
	}
}

donnderward_0004 = { #Caylen of Thunderward
	name = "Caylen"
	dynasty = dynasty_donnderward #
	religion = "iochand_cult"
	culture = "iochander"
	
	trait = race_human
	trait = education_intrigue_4
	trait = deceitful
	trait = paranoid
	trait = shy

	father = donnderward_0101

	985.6.4 = {
		birth = yes
	}
}

donnderward_0101 = { #dead Count Roen of Thunderward
	name = "Roen"
	dynasty = dynasty_donnderward #
	religion = "iochand_cult"
	culture = "iochander"
	
	trait = race_human
	
	947.6.6 = {
		birth = yes
	}
	
	990.2.3 = {
		death = "990.2.3"
	}
}

hooifield_0001 = { #Count Robin of Haysfield
	name = "Robin"
	dynasty = dynasty_hooifield #
	religion = "iochand_cult"
	culture = "iochander"
	
	trait = race_human
	trait = education_diplomacy_3
	trait = brave
	trait = arrogant
	trait = gregarious
	trait = strong

	father = hooifield_0101
	
	981.2.22 = {
		birth = yes
	}

	999.1.1 = {
		add_spouse = nimsnoms_0007
	}
}

hooifield_0002 = { #Belan of Haysfield
	name = "Belan"
	dynasty = dynasty_hooifield #
	religion = "iochand_cult"
	culture = "iochander"
	
	trait = race_human
	trait = education_stewardship_3
	trait = honest
	trait = impatient
	trait = gregarious

	father = hooifield_0001
	mother = nimsnoms_0007
	
	1000.12.15 = {
		birth = yes
	}
}

hooifield_0003 = { #Milian of Haysfield
	name = "Milian"
	dynasty = dynasty_hooifield #
	religion = "iochand_cult"
	culture = "iochander"
	female = yes
	
	trait = race_human
	trait = education_learning_2
	trait = content
	trait = craven
	trait = fickle

	father = hooifield_0001
	mother = nimsnoms_0007
	
	1003.7.1 = {
		birth = yes
	}
	1019.12.1 = {
		add_spouse =  carantis_0002
	}
}

hooifield_0004 = { #Aunnia of Haysfield
	name = "Aunnia"
	dynasty = dynasty_hooifield #
	religion = "iochand_cult"
	culture = "iochander"
	female = yes
	
	trait = race_human
	trait = education_martial_3
	trait = brave
	trait = greedy
	trait = cynical

	father = hooifield_0101
	
	990.1.29 = {
		birth = yes
	}
}

hooifield_0101 = { #dead Count Belan of Haysfield
	name = "Belan"
	dynasty = dynasty_hooifield #
	religion = "iochand_cult"
	culture = "iochander"

	trait = race_human
	
	953.4.20 = {
		birth = yes
	}
	
	1019.8.30 = {
		death = "1019.8.30"
	}
}

bluddythol_0001 = { #Countess Marie of Carverhold
	name = "Marie"
	dna = marie_bluddythol
	dynasty = dynasty_bluddythol #
	religion = "iochand_cult"
	culture = "iochander"
	female = yes
	
	trait = race_human
	trait = education_martial_3
	trait = just
	trait = honest
	trait = stubborn
	
	971.4.9 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}

	1021.1.1 = {
		give_council_position = councillor_marshal
	}
}

bluddythol_0002 = { #Adeline, Heir to Carverhold
	name = "Adeline"
	dynasty = dynasty_bluddythol #
	religion = "iochand_cult"
	culture = "iochander"
	female = yes

	mother = bluddythol_0001
	
	trait = race_human
	trait = education_martial_2
	trait = trusting
	trait = wrathful
	trait = athletic
	
	995.9.6 = {
		birth = yes
	}

	1021.1.1 = {
		give_council_position = councillor_marshal
	}
}

bluddythol_0003 = { #Sofia, bastard of Marie
	name = "Sofia"
	dna = sofia_bluddythol
	dynasty = dynasty_bluddythol #
	religion = "iochand_cult"
	culture = "iochander"
	sexuality = bisexual
	female = yes

	mother = bluddythol_0001
	
	trait = race_half_elf
	trait = education_learning_3
	trait = magical_affinity_2
	trait = ambitious
	trait = deceitful
	trait = patient
	
	1005.2.10 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}

	1021.1.1 = {
		give_council_position = councillor_spymaster
	}
}