﻿
sun_elvish0001 = { # Jaher
	name = "Jaher"
	dna = 23_jaher
	dynasty = dynasty_jaherzuir #Jaherzuir
	religion = "jaherian_cults"
	culture = "sun_elvish"
	
	diplomacy = 5
	martial = 10
	stewardship = 5
	intrigue = 8
	learning = 7
	prowess = 14
	
	trait = race_elf
	trait = education_martial_4
	trait = ambitious
	trait = arrogant
	trait = impatient
	trait = intellect_good_1
	trait = physique_good_3
	trait = beauty_good_2
	trait = magical_affinity_1
	trait = sun_reborn
	
	791.6.1 = {	#June 1st, aka the first day of our month Suren aka Sun's Start - almost as if hes the chosen one
		birth = yes
		if = {
			limit = {
				has_game_rule = enable_marked_by_destiny
			}
			add_character_modifier = { modifier = anb_marked_by_destiny_modifier }
		}
		effect = {
			add_character_flag = has_scripted_appearance
			add_character_flag = no_headgear
			
			set_artifact_rarity_illustrious = yes
			create_artifact = {
				name = dinatoldir_name
				description = dinatoldir_description
				type = spear
				template = dinatoldir_template
				visuals = spear # TODO
				wealth = scope:wealth
				quality = scope:quality
				history = {
					type = created_before_history
					recipient = character:sun_elvish0001
				}
				modifier = dinatoldir_modifier
				save_scope_as = newly_created_artifact_1
			}
			scope:newly_created_artifact_1 = {
				add_artifact_history = {
					recipient = character:sun_elvish0001
					type = inherited
					date = 801.6.1
				}
				set_variable = { name = historical_unique_artifact value = yes }
				set_variable = is_dinatoldir # Do not add this for all artifacts, only those with on_action effects need it
			}
			set_artifact_rarity_illustrious = yes
			create_artifact = {
				name = flag_of_the_diranhria_name
				description = flag_of_the_diranhria_description
				type = wall_big
				template = flag_of_the_diranhria_template
				visuals = banner
				visuals_source = title:e_bulwar # placeholder until we have something
				wealth = scope:wealth
				quality = scope:quality
				history = {
					type = created_before_history
					recipient = character:sun_elvish0001
				}
				modifier = flag_of_the_diranhria_modifier
				save_scope_as = newly_created_artifact_2
				decaying = no
			}
			scope:newly_created_artifact_2 = {
				set_variable = { name = historical_unique_artifact value = yes }
			}
		}
	}
	
	992.5.1 = {
		add_spouse = sun_elvish0004 # Erelmis the Chaste
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0014 # Shaeranni the Dancer
	}
	
	1005.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0010 # Ariathra the Beautiful
	}
	
	1015.09.07 = {
		add_spouse = 168 # Lanahari ta'Luneteín
	}

	1127.12.21 = {
		death = {
			death_reason = death_murder
		}
	}
}

sun_elvish0002 = { # Jaerel
	name = "Jaerel"
	dynasty = dynasty_jaherzuir #Jaherzuir
	religion = "jaherian_cults"
	culture = "sun_elvish"
	
	trait = race_elf
	trait = education_diplomacy_2
	trait = just
	trait = compassionate
	trait = trusting
	trait = sun_reborn_descendant

	father = sun_elvish0001 # Jaher
	mother = sun_elvish0004 # Erelmis the Chaste

	995.5.1 = {
		if = {
			limit = {
				has_game_rule = enable_marked_by_destiny
			}
			add_character_modifier = { modifier = anb_marked_by_destiny_modifier }
		}
		birth = yes
	}

	1137.12.21 = {
		death = {
			death_reason = death_murder
		}
	}
}

sun_elvish0003 = { # Elisar
	name = "Elisar"
	dynasty = dynasty_jaherzuir #Jaherzuir
	religion = "jaherian_cults"
	culture = "sun_elvish"
	
	trait = race_elf
	trait = education_martial_4
	trait = wrathful
	trait = paranoid
	trait = sadistic
	trait = lunatic_1
	trait = aggressive_attacker
	trait = sun_reborn_descendant
	
	father = sun_elvish0001 # Jaher
	mother = sun_elvish0014 # Shaeranni the Dancer
	
	1004.7.8 = {
		if = {
			limit = {
				has_game_rule = enable_marked_by_destiny
			}
			add_character_modifier = { modifier = anb_marked_by_destiny_modifier }
		}
		birth = yes
	}

	1173.8.1 = {
		death = yes
	}
}

sun_elvish0004 = { # Erelmis the Chaste, first wife of Jaher, mother of Jaerel
	name = "Erelmis"
	female = yes
	religion = "jaherian_cults"
	culture = "sun_elvish"
	
	trait = race_elf_dead
	trait = beauty_good_3
	trait = chaste
	trait = humble
	trait = compassionate
	
	792.2.1 ={
		birth = yes
	}
	
	992.5.1 = {
		add_spouse = sun_elvish0001 # Jaher
		give_nickname = nick_the_chaste
	}
	
	1001.9.1 = {
		death = yes
	}
}

sun_elvish0005 = { # Denarion Denarzuir, governor of Corvuria
	name = "Denarion"
	dna = 113_denarion_denarzuir
	dynasty = dynasty_denarzuir	#Denarzuir
	religion = "jaherian_cults"
	culture = "sun_elvish"
	
	diplomacy = 5
	martial = 7
	stewardship = 10
	intrigue = 4
	learning = 4
	prowess = 9	
	
	trait = race_elf
	trait = education_stewardship_4
	trait = arrogant
	trait = temperate
	trait = diligent
	
	783.6.2 = {
		birth = yes
		add_character_flag = has_scripted_appearance
	}
}

sun_elvish0006 = { # Eledas I Sarelzuir, governor of Sareyand
	name = "Eledas"
	dynasty = dynasty_sarelzuir
	religion = jaherian_cults
	culture = sun_elvish
	
	trait = race_elf
	trait = education_learning_3
	trait = diligent
	trait = arrogant
	trait = generous
	
	925.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0009
	}
}

sun_elvish0007 = { # Eledas II Sarelzuir
	name = "Eledas"
	dynasty = dynasty_sarelzuir
	religion = jaherian_cults
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	trait = twin
	
	father = sun_elvish0006
	mother = sun_elvish0009
	
	1019.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

sun_elvish0008 = { # Panoril Sarelzuir
	name = "Panoril"
	dynasty = dynasty_sarelzuir
	religion = jaherian_cults
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	trait = twin
	
	father = sun_elvish0006
	mother = sun_elvish0009
	
	1019.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

sun_elvish0009 = { # Varilla, wife of Eledas I, mother of Eledas II
	name = "Varilla"
	# lowborn
	religion = jaherian_cults
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	878.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0006
	}
}

sun_elvish0010 = { # Ariathra the Beautiful, sister of Eledas I, third wife of Jaher
	name = "Ariathra"
	dynasty = dynasty_sarelzuir
	religion = jaherian_cults
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	trait = beauty_good_3
	
	950.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1005.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0001 # Jaher
	}
}

# add a parent for Eledas and Ariathra

sun_elvish0011 = { # Dorendor Olorzuir, General of Jaher
	name = "Dorendor"
	dynasty = dynasty_olorzuir
	religion = jaherian_cults
	culture = sun_elvish
	
	trait = race_elf
	trait = education_martial_4
	trait = brave
	trait = patient
	trait = wrathful
	trait = military_engineer
	# trait = giant # marked with ? in the doc
	
	878.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
		# rezangal shield artifact
	}
	
	1022.1.1 = {
		employer = sun_elvish0001
	}
}

sun_elvish0012 = { # Vulzin "the Red" Vulzinzuir, future governor of Elizna
	name = "Vulzin"
	dynasty = dynasty_vulzinzuir
	religion = jaherian_cults
	culture = sun_elvish
	
	trait = race_elf
	# trait = education_stewardship_
	trait = arbitrary
	trait = vengeful
	trait = content
	
	830.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
		give_nickname = nick_the_red
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0013
	}
}

sun_elvish0013 = { # Liandiel, wife of Vulzin
	name = "Liandiel"
	# lowborn
	religion = jaherian_cults
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	900.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0012
	}
}

sun_elvish0014 = { # Shaeranni the Dancer, sister of Vulzin, second wife of Jaher, Elisar's mother
	name = "Shaeranni"
	dynasty = dynasty_vulzinzuir
	religion = jaherian_cults
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	trait = fickle
	trait = impatient
	trait = gregarious
	trait = lifestyle_reveler
	
	904.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
		give_nickname = nick_the_dancer
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0001 # Jaher
		
		add_trait_xp = {
			trait = lifestyle_reveler
			value = 100
		}
	}
}

# add a parent for Vulzin and Shaeranni

sun_elvish0017 = { # Alvarion Alvazuir
	name = "Alvarion"
	dynasty = dynasty_alvazuir
	religion = jaherian_cults
	culture = sun_elvish
	
	trait = race_elf
	# trait = education_intrigue_
	trait = lustful
	trait = arrogant
	trait = calm
	
	838.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0021
	}
}

sun_elvish0018 = { # Elaressa Alvazuir, future governor of Azka-Sur
	name = "Elaressa"
	dynasty = dynasty_alvazuir
	religion = jaherian_cults
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education_stewardship_
	trait = content
	trait = zealous
	trait = generous
	
	father = sun_elvish0017
	mother = sun_elvish0021
	
	911.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

sun_elvish0019 = { # Darastarion Alvazuir
	name = "Darastarion"
	dynasty = dynasty_alvazuir
	religion = jaherian_cults
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	father = sun_elvish0017
	mother = sun_elvish0021
	
	1007.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

sun_elvish0020 = { # Ultarion Alvazuir
	name = "Ultarion"
	dynasty = dynasty_alvazuir
	religion = jaherian_cults
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	father = sun_elvish0017
	mother = sun_elvish0021
	
	1018.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

sun_elvish0021 = { # Alarawel, wife of Alvarion
	name = "Alarawel"
	# lowborn
	religion = jaherian_cults
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	810.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0017
	}
}

sun_elvish0022 = { # Gelmonias Dalzuir, future governor of Dalarand
	name = "Gelmonias"
	dynasty = dynasty_dalzuir
	religion = jaherian_cults
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	866.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0024
	}
}

sun_elvish0023 = { # Darastarion Dalzuir
	name = "Darastarion"
	dynasty = dynasty_dalzuir
	religion = jaherian_cults
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	father = sun_elvish0022
	mother = sun_elvish0024
	
	1004.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

sun_elvish0024 = { # Panoril, wife of Gelmonias
	name = "Panoril"
	# lowborn
	religion = jaherian_cults
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	840.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0022
	}
}

sun_elvish0025 = { # Varamel Varamzuir, Count of Varamar (vassal of Gelmonias)
	name = "Varamel"
	dynasty = dynasty_varamzuir
	religion = jaherian_cults
	culture = sun_elvish
	
	trait = race_elf
	# trait = education

	841.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0027
	}
}

sun_elvish0026 = { # Galindil Varamzuir
	name = "Galindil"
	dynasty = dynasty_varamzuir
	religion = jaherian_cults
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	father = sun_elvish0025
	mother = sun_elvish0027
	
	1011.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

sun_elvish0027 = { # Eranil, wife of Varamel
	name = "Eranil"
	# lowborn
	religion = jaherian_cults
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	874.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0025
	}
}

sun_elvish0028 = { # Irrlion Irrliazuir, father of Taelarios (not born), count of Irrliam
	name = "Irrlion"
	dynasty = dynasty_irrliazuir
	religion = jaherian_cults
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	trait = zealous
	trait = humble
	trait = lazy
	
	800.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0029
	}
}

sun_elvish0029 = { # Imariel, wife of Irrlion
	name = "Imariel"
	# lowborn
	religion = jaherian_cults
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	840.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0028
	}
}

sun_elvish0030 = { # Uyel Uyelzuir, governor of Re’uyel
	name = "Uyel"
	dynasty = dynasty_uyelzuir
	religion = jaherian_cults
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education_stewardship_
	trait = arrogant
	trait = greedy
	trait = deceitful
	
	842.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_matrilineal_spouse = sun_elvish0031
	}
}

sun_elvish0031 = { # Kalinael, husband of Uyel
	name = "Kalinael"
	# lowborn
	religion = jaherian_cults
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	784.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_matrilineal_spouse = sun_elvish0030
	}
}

sun_elvish0032 = { # Kalinael Uyelzuir
	name = "Kalinael"
	dynasty = dynasty_uyelzuir
	religion = jaherian_cults
	culture = sun_elvish
	
	trait = race_elf
	# trait = education_intrigue_
	trait = deceitful
	trait = ambitious
	trait = calm
	
	father = sun_elvish0031
	mother = sun_elvish0030
	
	995.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

sun_elvish0033 = { # Veharia Uyelzuir
	name = "Veharia"
	dynasty = dynasty_uyelzuir
	religion = jaherian_cults
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	father = sun_elvish0031
	mother = sun_elvish0030
	
	1021.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
}

sun_elvish0034 = { # Veharia Lezuir, governor of Ginerdu
	name = "Veharia"
	dynasty = dynasty_lezuir
	religion = jaherian_cults
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	899.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_matrilineal_spouse = sun_elvish0035
	}
}

sun_elvish0035 = { # Talanor, lowborn, husband of Veharia
	name = "Talanor"
	# lowborn
	religion = jaherian_cults
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	912.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_matrilineal_spouse = sun_elvish0034
	}
}

sun_elvish0036 = { # Firinar Aralzuir, governor of Kalib
	name = "Firinar"
	dynasty = dynasty_aralzuir
	religion = jaherian_cults
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	912.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0037
	}
	1018.3.4 = { 
		employer = sun_elvish0001
	}
}

sun_elvish0037 = { # Iztralania, lowborn, wife of Firinar
	name = "Iztralania"
	# lowborn
	religion = jaherian_cults
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	847.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0036
	}
}

sun_elvish0038 = { # Thelrion Nestezuir, future governor of Setadazar
	name = "Thelrion"
	dynasty = dynasty_nestezuir
	religion = jaherian_cults
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	850.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0039
	}
}

sun_elvish0039 = { # Lezlindel, lowborn, wife of Thelrion
	name = "Lezlindel"
	# lowborn
	religion = jaherian_cults
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	899.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0038
	}
}

sun_elvish0040 = { # Thranduir Tirenzuir, governor of Busilar
	name = "Thranduir"
	dynasty = dynasty_tirenzuir
	religion = jaherian_cults
	culture = sun_elvish
	
	trait = race_elf
	# trait = education
	
	901.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0041
	}
	
	1021.12.31 = {
		set_realm_capital = title:c_port_jaher
	}
}

sun_elvish0041 = { # Alarawel, lowborn, wife of Thranduir
	name = "Alarawel"
	# lowborn
	religion = jaherian_cults
	culture = sun_elvish
	female = yes
	
	trait = race_elf
	# trait = education
	
	921.1.1 = { # correct year, random date PLACEHOLDER
		birth = yes
	}
	
	1000.1.1 = { # random date PLACEHOLDER
		add_spouse = sun_elvish0040
	}
}

#azka evran 

evranzuir_0001 = { # Evran Evranzuir, commander of Azka Evran
	name = "Evran"
	dynasty = dynasty_evranzuir
	religion = sundancer_paragons
	culture = sun_elvish
	disallow_random_traits = yes

	father = evranzuir_0005
	mother = evranzuir_0006
	
	trait = race_elf
	trait = education_martial_3
	trait = brave
	trait = wrathful
	trait = zealous
	trait = faith_warrior
	trait = lifestyle_gardener
	trait = intellect_good_1
	trait = lifestyle_blademaster
	trait = one_eyed
	trait = loyal
	
	879.4.13 = { 
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
	
	1018.3.4 = { 
		add_spouse = evranzuir_0002
		effect = {
			set_relation_friend = character:evranzuir_0011
		}
	}
	
}

evranzuir_0002 = { # Liandel, Evran's wife
	name = "Liandel"
	religion = sundancer_paragons
	culture = sun_elvish
	female = yes
	disallow_random_traits = yes
	
	trait = race_elf
	trait = education_learning_3
	trait = fickle
	trait = wrathful
	trait = temperate
	trait = faith_warrior
	trait = lifestyle_herbalist
	trait = lifestyle_physician
	trait = beauty_good_2 
	trait = magical_affinity_2
	trait = albino
	trait = reclusive
	
	895.7.21 = { 
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
	
	1018.3.4 = { 
		add_spouse = evranzuir_0001
		effect = {
			set_relation_lover = character:evranzuir_0001
		}
		effect = {
			set_relation_friend = character:evranzuir_0006
		}
	}
	
}

evranzuir_0003 = { # Zaria, Evran's daugther
	name = "Zaria"
	dynasty = dynasty_evranzuir
	religion = sundancer_paragons
	culture = sun_elvish
	female = yes
	disallow_random_traits = yes

	father = evranzuir_0001
	mother = evranzuir_0002

	trait = race_elf
	trait = magical_affinity_2
	trait = intellect_good_1
	trait = twin

	1019.11.13 = { 
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
}

evranzuir_0004 = { # Erendil, Evran's son
	name = "Erendil"
	dynasty = dynasty_evranzuir
	religion = sundancer_paragons
	culture = sun_elvish
	disallow_random_traits = yes

	father = evranzuir_0001
	mother = evranzuir_0002

	trait = race_elf
	trait = beauty_good_2 
	trait = twin

	1019.11.13 = { 
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
}

evranzuir_0005 = { # Olorion, Evran's father, dead
	name = "Olorion"
	dynasty = dynasty_evranzuir
	religion = sundancer_paragons
	culture = sun_elvish
	disallow_random_traits = yes

	father = evranzuir_0009
	
	trait = race_elf_dead
	trait = education_martial_2
	trait = brave
	trait = compassionate
	trait = honest
	trait = faith_warrior
	trait = intellect_good_1
	
	785.7.24 = { 
		birth = yes
	}
	
	873.5.12 = { 
		add_spouse = evranzuir_0006
		effect = {
			set_variable = no_purist_trait
		}
	}
	1005.7.25 = {
		death = yes
	}
}

evranzuir_0006 = { # filnara, evran's mother
	name = "Filnara"
	religion = jaherian_cults
	culture = sun_elvish
	female = yes
	disallow_random_traits = yes
	
	trait = race_elf
	trait = education_intrigue_4
	trait = deceitful
	trait = patient
	trait = calm
	trait = lifestyle_herbalist
	trait = lifestyle_gardener
	
	800.1.28 = { 
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
	
	873.5.12 = { 
		add_spouse = evranzuir_0005
	}
	
}

evranzuir_0007 = { # Imariel, Evran's sister
	name = "Imariel"
	dynasty = dynasty_evranzuir
	religion = sundancer_paragons
	culture = sun_elvish
	female = yes
	disallow_random_traits = yes

	father = evranzuir_0005
	mother = evranzuir_0006
	
	trait = race_elf
	trait = education_diplomacy_2
	trait = gregarious
	trait = calm
	trait = patient
	trait = lifestyle_gardener
	trait = intellect_good_1
	trait = lifestyle_reveler 
	
	911.10.5 = { 
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
	#can be used as a spouse for another character
	
}

evranzuir_0008 = { # Zerondor, Evran's brother
	name = "Zerondor"
	dynasty = dynasty_evranzuir
	religion = sundancer_paragons
	culture = sun_elvish
	disallow_random_traits = yes

	father = evranzuir_0005
	mother = evranzuir_0006
	
	trait = race_elf
	trait = education_martial_3
	trait = gregarious
	trait = honest
	trait = zealous
	trait = lifestyle_gardener
	trait = intellect_good_1
	trait = lifestyle_hunter
	
	1002.6.13 = { 
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
	
}

evranzuir_0009 = { # Ultarion, Evran's granfather, dead
	name = "Ultarion"
	dynasty = dynasty_evranzuir
	religion = sundancer_paragons
	culture = sun_elvish
	disallow_random_traits = yes
	
	trait = race_elf_dead
	trait = education_learning_2
	trait = gregarious
	trait = just
	trait = forgiving
	trait = intellect_good_1
	
	511.2.15 = { 
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}

	950.7.30 = {
		death = yes
	}
}

evranzuir_0010 = { # Aldarian, Evran's uncle
	name = "Aldarian"
	dynasty = dynasty_evranzuir
	religion = sundancer_paragons
	culture = sun_elvish
	disallow_random_traits = yes

	father = evranzuir_0009
	
	trait = race_elf
	trait = education_diplomacy_2
	trait = stubborn
	trait = compassionate
	trait = temperate
	trait = blind
	trait = intellect_good_1
	
	640.5.12 = { 
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
	
}

evranzuir_0011 = { # Kelador, Evran's cousin
	name = "Kelador"
	dynasty = dynasty_evranzuir
	religion = sundancer_paragons
	culture = sun_elvish
	disallow_random_traits = yes

	father = evranzuir_0010
	
	trait = race_elf
	trait = education_stewardship_3
	trait = honest
	trait = content
	trait = patient
	trait = lifestyle_gardener
	trait = one_legged
	trait = intellect_good_1
	trait = faith_warrior
	trait = architect
	
	807.2.22 = { 
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
}

#Sartazuir
sartazuir_0001 = {
	name = "Siridan"
	dynasty = dynasty_sartazuir
	religion = sundancer_paragons
	culture = sun_elvish
	dna = dna_siridan_sartazuir
	disallow_random_traits = yes

	trait = race_elf
 	trait = education_learning_3
	trait = humble
	trait = diligent
	trait = compassionate
	trait = architect
	trait = lifestyle_gardener
	trait = magical_affinity_3

	diplomacy = 6
	martial = 16
	stewardship = 11
	intrigue = 3
	learning = 5
	prowess = 10

	931.03.15 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
			set_variable = no_purist_trait
		}
	}

	1020.2.18 = {
		add_spouse = amarienzuir0005 #Ariathra Amarienzuir
	}

	portrait_override={
		portrait_modifier_overrides={
			custom_beards=male_empty
			custom_hair=male_hair_fp1_02
			custom_clothes=male_clothes_secular_fp2_iberian_muslim_common_01
			custom_cloaks=male_empty
			custom_headgear=male_headgear_secular_fp2_iberian_muslim_common_01_low
			custom_legwear=male_legwear_secular_mena_common_01
		}
	}
}

sartazuir_0002 = {
	name = "Artael"
	dynasty = dynasty_sartazuir
	religion = sundancer_paragons
	culture = sun_elvish
	father = sartazuir_0001
	mother = amarienzuir0005
	disallow_random_traits = yes

	trait = race_elf

	1021.3.9 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
}

#Amarienzuir
amarienzuir0001 = { # Amarien Amarienzuir, governor of Imulushes, Jaher's best friend
	name = "Amarien"
	dynasty = dynasty_amarienzuir
	religion = sundancer_paragons
	culture = sun_elvish
	female = yes
	dna = dna_amarien_amarienzuir
	father = amarienzuir0003
	mother = amarienzuir0004
	disallow_random_traits = yes

	trait = race_elf
	trait = education_martial_4
	trait = brave
	trait = cynical
	trait = shy
	trait = strong
	trait = shieldmaiden

	diplomacy = 3
	martial = 10
	stewardship = 7
	intrigue = 5
	learning = 6
	prowess = 17
	
	789.11.18 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
	
	876.6.21 = { #
		set_relation_best_friend = character:sun_elvish0001 # Jaher
	}
	
	1008.9.17 = {
		add_matrilineal_spouse = amarienzuir0002 #Artorian
	}

	portrait_override={
		portrait_modifier_overrides={
			custom_beards=female_empty
			custom_cloaks=female_empty
			custom_hair=female_hair_northern_02
			custom_clothes=f_clothes_sec_mena_war_nob_01_hi
			custom_headgear=female_empty
			custom_legwear=female_legwear_secular_mena_nobility_01
		}
	}
}

amarienzuir0002 = { # Artorian, husband of Amarien
	name = "Artorian"
	religion = sundancer_paragons
	culture = sun_elvish
	disallow_random_traits = yes
	
	trait = race_elf
	trait = education_learning_3
	trait = diligent
	trait = stubborn
	trait = honest
	trait = scholar

	diplomacy = 7
	martial = 9
	stewardship = 6
	intrigue = 6
	learning = 9
	prowess = 9
	
	841.8.7 = { 
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
}

amarienzuir0003 = { # Alarian Amarienzuir, Father of Amarien
	name = "Alarian"
	dynasty = dynasty_amarienzuir
	religion = sundancer_paragons
	culture = sun_elvish
	disallow_random_traits = yes
	
	trait = race_elf_dead
	trait = education_martial_2
	
	657.9.12 = {
		birth = yes
	}
	
	1000.1.1 = { #Great Storm, Correct year, placeholder date
		death = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
}

amarienzuir0004 = { # Keladora Amarienzuir, Mother of Amarien
	name = "Keladora"
	religion = sundancer_paragons
	culture = sun_elvish
	female = yes
	disallow_random_traits = yes
	
	trait = race_elf
	trait = education_stewardship_2
	trait = craven
	trait = generous
	trait = diligent
	
	663.10.17 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
	
	788.5.2 = {
		add_spouse = amarienzuir0003
	}
}

amarienzuir0005 = { # Ariathra Amarienzuir, Amarien's Sister
	name = "Ariathra"
	dynasty = dynasty_amarienzuir
	religion = sundancer_paragons
	culture = sun_elvish
	female = yes
	father = amarienzuir0003
	mother = amarienzuir0004
	disallow_random_traits = yes
	
	trait = race_elf
	trait = education_martial_2
	trait = eccentric
	trait = brave
	trait = temperate
	trait = flexible_leader

	diplomacy = 8
	martial = 9
	stewardship = 8
	intrigue = 2
	learning = 6
	prowess = 17
	
	928.06.2 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
}

amarienzuir0006 = { # Darandil Amarienzuir, Amarien's Son
	name = "Darandil"
	dynasty = dynasty_amarienzuir
	religion = sundancer_paragons
	culture = sun_elvish
	father = amarienzuir0001
	mother = amarienzuir0002
	disallow_random_traits = yes
	
	trait = race_elf
	trait = education_martial_2
	trait = impatient
	trait = arrogant
	trait = zealous
	trait = aggressive_attacker

	diplomacy = 8
	martial = 7
	stewardship = 7
	intrigue = 4
	learning = 5
	prowess = 13
	
	912.7.28 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
		}
	}
}