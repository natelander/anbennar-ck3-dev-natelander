﻿
araskaysit0001 = { #Dariuš Araškaysit, King of Akalšes
	name = "Darius_"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	dna = dna_darius_araskaysit
	father = araskaysit0003
	disallow_random_traits = yes

	trait = race_human
 	trait = education_learning_3
	trait = eccentric
	trait = paranoid
	trait = arrogant
	trait = august
	trait = intellect_good_2
	trait = magical_affinity_2

	diplomacy = 6
	martial = 1
	stewardship = 4
	intrigue = 5
	learning = 6
	prowess = 4

	979.10.18 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			add_character_flag = has_scripted_appearance
		}
	}

	portrait_override ={
		portrait_modifier_overrides={
			custom_hair=m_hair_fp3_iranian_03
			custom_clothes=m_clothes_sec_fp3_iranian_nob_02_hi
			custom_headgear=m_headgear_sec_fp3_iranian_roy_01
			custom_beards=m_beard_fp3_iranian_01
		}
	}
}

araskaysit0002 = { #Parissa Araškaysit
	name = "Parissa"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_parissa_araskaysit
	female = yes
	father = araskaysit0003
	disallow_random_traits = yes

	trait = race_human
 	trait = education_stewardship_4
	trait = gregarious
	trait = compassionate
	trait = calm
	trait = administrator

	diplomacy = 8
	martial = 3
	stewardship = 8
	intrigue = 6
	learning = 14
	prowess = 5

	987.12.9 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_hair=m_hair_fp3_iranian_03
	#		custom_clothes=m_clothes_sec_fp3_iranian_nob_02_hi
	#		custom_headgear=m_headgear_sec_fp3_iranian_roy_01
	#		custom_beards=m_beard_fp3_iranian_01
	#	}
	#}
}

araskaysit0003 = { #Kawad VII Araškaysit
	name = "Kawad"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_kawad_VII_araskaysit
	father = araskaysit0004
	disallow_random_traits = yes

	trait = race_human

	945.4.12 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	987.9.3 = {
		death = yes
	}

	portrait_override ={
		portrait_modifier_overrides={
			custom_clothes=m_clothes_sec_fp3_iranian_nob_02_hi
			custom_headgear=m_headgear_sec_fp3_iranian_roy_01
		}
	}
}

araskaysit0004 = { #Kawad VI Araškaysit
	name = "Kawad"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_kawad_VI_araskaysit
	disallow_random_traits = yes
	mother = araskaysit0008

	trait = race_human

	922.6.9 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	983.1.6 = {
		death = yes
	}

	portrait_override ={
		portrait_modifier_overrides={
			custom_clothes=m_clothes_sec_fp3_iranian_nob_01_hi
			custom_headgear=m_headgear_sec_fp3_iranian_roy_01
		}
	}
}

araskaysit0005 = { # Serakh Araškaysit
	name = "Serakh"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes

	trait = race_human

	872.12.3 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	913.1.12 = {
		death = yes
	}

	portrait_override ={
		portrait_modifier_overrides={
			custom_clothes=m_clothes_sec_fp3_iranian_nob_01_hi
			custom_headgear=m_headgear_sec_fp3_iranian_roy_01
		}
	}
}

araskaysit0006 = { # Ardaš Araškaysit
	name = "Ardas_"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	father = araskaysit0005

	trait = race_human

	891.6.1 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	933.7.19 = {
		death = yes
	}

	portrait_override ={
		portrait_modifier_overrides={
			custom_clothes=m_clothes_sec_fp3_iranian_nob_01_hi
			custom_headgear=m_headgear_sec_fp3_iranian_roy_01
		}
	}
}

araskaysit0007 = { # Andar Araškaysit
	name = "Andar"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	father = araskaysit0005

	trait = race_human

	895.6.12 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	946.3.17 = {
		death = yes
	}

	portrait_override ={
		portrait_modifier_overrides={
			custom_clothes=m_clothes_sec_fp3_iranian_nob_01_hi
			custom_headgear=m_headgear_sec_fp3_iranian_roy_01
		}
	}
}

araskaysit0008 = { # Aruhana Araškaysit
	name = "Aruhana"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	father = araskaysit0005
	female = yes

	trait = race_human

	899.5.7 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	941.4.12 = {
		death = yes
	}

	portrait_override ={
		portrait_modifier_overrides={
			custom_clothes=m_clothes_sec_fp3_iranian_nob_01_hi
			custom_headgear=m_headgear_sec_fp3_iranian_roy_01
		}
	}
}

araskaysit0009 = { # Arwia Araškaysit
	name = "Arwia"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	father = araskaysit0006
	female = yes

	trait = race_human

	913.2.17 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	971.5.12 = {
		death = yes
	}

	portrait_override ={
		portrait_modifier_overrides={
			custom_clothes=m_clothes_sec_fp3_iranian_nob_01_hi
			custom_headgear=m_headgear_sec_fp3_iranian_roy_01
		}
	}
}

araskaysit0010 = { # Gurdar Araškaysit
	name = "Gurdar"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	father = araskaysit0006

	trait = race_human

	914.8.10 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	950.2.24 = {
		death = yes
	}
}

araskaysit0011 = { # Takhmasp Araškaysit
	name = "Takhmasp"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	mother = araskaysit0008

	trait = race_human

	917.4.8 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	959.3.19 = {
		death = yes
	}
}

araskaysit0012 = { # Karuš Araškaysit
	name = "Karus_"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	mother = araskaysit0009

	trait = race_human

	936.11.2 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	982.12.9 = {
		death = yes
	}
}

araskaysit0013 = { # Rušana Araškaysit
	name = "Rus_ana"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	mother = araskaysit0009
	female = yes

	trait = race_human

	939.1.21 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	992.7.15 = {
		death = yes
	}
}

araskaysit0014 = { # Arwand Araškaysit
	name = "Arwand"
	dynasty = dynasty_araskaysit
	religion = "cult_of_the_law"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	mother = araskaysit0010

	trait = race_human

	938.5.28 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	995.3.7 = {
		death = yes
	}
}

araskaysit0015 = { # Sarfaz Araškaysit
	name = "Sarfaz"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	father = araskaysit0011

	trait = race_human

	937.5.16 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	979.2.28 = {
		death = yes
	}
}

araskaysit0016 = { # Samira Araškaysit ALIVE
	name = "Samira"
	dynasty = dynasty_araskaysit
	religion = "cult_of_the_law"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	father = araskaysit0012
	female = yes

	trait = race_human

	969.6.17 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	1025.3.24 = {
		death = yes
	}
}

araskaysit0017 = { # Širin Araškaysit ALIVE
	name = "S_irin"
	dynasty = dynasty_araskaysit
	religion = "cult_of_the_law"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	mother = araskaysit0013
	female = yes

	trait = race_human

	966.8.3 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	1029.3.4 = {
		death = yes
	}

	portrait_override ={
		portrait_modifier_overrides={
			custom_clothes=m_clothes_sec_fp3_iranian_nob_01_hi
			custom_headgear=m_headgear_sec_fp3_iranian_roy_01
		}
	}
}

araskaysit0018 = { # Aylara Araškaysit
	name = "Aylara"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	father = araskaysit0015
	female = yes

	trait = race_human

	961.5.2 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	1009.4.5 = {
		death = yes
	}
}

araskaysit0019 = { # Pireez Araškaysit ALIVE
	name = "Pireez"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	father = araskaysit0015

	trait = race_human

	964.11.12 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	1023.6.21 = {
		death = yes
	}
}

araskaysit0020 = { # Semiha Araškaysit ALIVE
	name = "Semiha"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	father = araskaysit0003
	female = yes

	trait = race_human

	977.12.7 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	1059.6.3 = {
		death = yes
	}
}

araskaysit0021 = { # Ardaš Araškaysit ALIVE
	name = "Ardas_"
	dynasty = dynasty_araskaysit
	religion = "cult_of_the_law"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	mother = araskaysit0016

	trait = race_human

	982.4.12 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	1043.4.18 = {
		death = yes
	}
}

araskaysit0022 = { # Andar Araškaysit ALIVE
	name = "Andar"
	dynasty = dynasty_araskaysit
	religion = "cult_of_the_law"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	mother = araskaysit0017

	trait = race_human

	990.12.3 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	1038.5.8 = {
		death = yes
	}
}

araskaysit0023 = { # Ardašir Araškaysit ALIVE
	name = "Ardas_ir"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	mother = araskaysit0018

	trait = race_human

	982.9.7 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	1029.3.19 = {
		death = yes
	}
}

araskaysit0024 = { # Rimuš Araškaysit ALIVE
	name = "Rimus_"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	father = araskaysit0001

	trait = race_human

	1001.6.3 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	1035.2.15 = {
		death = yes
	}
}

araskaysit0025 = { # Kawad Araškaysit ALIVE
	name = "Kawad"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	father = araskaysit0001

	trait = race_human

	1003.8.12 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	1035.2.15 = {
		death = yes
	}
}

araskaysit0026 = { # Aylara Araškaysit ALIVE
	name = "Aylara"
	dynasty = dynasty_araskaysit
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__araskaysit
	disallow_random_traits = yes
	father = araskaysit0023
	female = yes

	trait = race_human

	1001.7.19 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	1063.4.12 = {
		death = yes
	}
}

ruhagar0001 = { #Serakh Ruhagar
	name = "Serakh"
	dynasty = dynasty_ruhagar
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_serakh_ruhagar
	disallow_random_traits = yes

	trait = race_human
 	trait = education_stewardship_2

	934.5.6 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	997.2.13 = {
		death = yes
	}
}

ruhagar0002 = { #Zamman Ruhagar, Lord Ruhagar
	name = "Zamman"
	dynasty = dynasty_ruhagar
	religion = "way_of_kings"
	culture = "akalsesi"
	dna = dna_zamman_ruhagar
	father = ruhagar0001
	disallow_random_traits = yes

	trait = race_human
 	trait = education_martial_3
	trait = vengeful
	trait = wrathful
	trait = brave
	trait = family_first

	diplomacy = 8
	martial = 5
	stewardship = 9
	intrigue = 6
	learning = 11
	prowess = 15

	956.3.18 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			add_character_flag = has_scripted_appearance
		}
	}

	980.3.1 = {
		add_spouse = taumakar0002 #Asiah Taumakar
	}

	1001.1.1 = {
		give_nickname = nick_s_erpahn
	}

	portrait_override ={
		portrait_modifier_overrides={
			custom_beards=m_beard_fp3_iranian_02
			custom_hair=male_hair_fp2_iberian_muslim_02
			custom_clothes=m_clothes_sec_fp3_iranian_war_nob_01
			custom_headgear=m_headgear_sec_fp3_iranian_nob_01
		}
	}
}

ruhagar0003 = { #Zarhran Ruhagar
	name = "Zarhran"
	dynasty = dynasty_ruhagar
	religion = "way_of_kings"
	culture = "akalsesi"
	dna = dna_zarhran_ruhagar
	father = ruhagar0002
	disallow_random_traits = yes

	trait = race_human
 	trait = education_martial_4
	trait = temperate
	trait = diligent
	trait = gregarious
	trait = lifestyle_blademaster

	 diplomacy = 6
	 martial = 4
	 stewardship = 5
	 intrigue = 5
	 learning = 6
	 prowess = 12

	981.12.7 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			add_character_flag = has_scripted_appearance
		}
	}

	1002.3.6 = {
		add_spouse = araskaysit0002 #Parissa Araškaysit
		give_nickname = nick_rakzigim
	}

	portrait_override={
		portrait_modifier_overrides={
			custom_legwear=male_legwear_secular_mena_nobility_01
			custom_hair=male_hair_fp2_iberian_muslim_02
			custom_clothes=m_clothes_sec_fp3_iranian_war_nob_01
			custom_headgear=m_headgear_sec_fp3_iranian_nob_02
			custom_beards=male_beard_indian_01
		}
	}
}

ruhagar0004 = { #Idris Ruhagar
	name = "Idris"
	dynasty = dynasty_ruhagar
	religion = "way_of_kings"
	culture = "akalsesi"
	female = yes
	#dna = dna_idris_ruhagar
	father = ruhagar0002
	disallow_random_traits = yes

	trait = race_human
 	trait = education_stewardship_2
	trait = greedy
	trait = gregarious
	trait = paranoid

	 diplomacy = 11
	 martial = 5
	 stewardship = 9
	 intrigue = 10
	 learning = 11
	 prowess = 5

	984.5.4 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#	}
	#}
}

ruhagar0005 = { #Serim Ruhagar
	name = "Serim"
	dynasty = dynasty_ruhagar
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_serim_ruhagar
	father = ruhagar0003
	disallow_random_traits = yes

	trait = race_human
 	trait = education_learning_1
	trait = chaste
	trait = callous
	trait = paranoid
	trait = possessed_1

	 diplomacy = 6
	 martial = 5
	 stewardship = 9
	 intrigue = 13
	 learning = 12
	 prowess = 2

	1003.2.19 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#	}
	#}
}

ruhagar0006 = { #Karuš Ruhagar
	name = "Karus_"
	dynasty = dynasty_ruhagar
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_karus_ruhagar
	father = ruhagar0003
	disallow_random_traits = yes

	trait = race_human
 	trait = education_martial_2
	trait = arrogant
	trait = honest
	trait = just

	 diplomacy = 11
	 martial = 17
	 stewardship = 16
	 intrigue = 0
	 learning = 6
	 prowess = 9

	1005.6.9 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#	}
	#}
}

ruhagar0007 = { #Azura Ruhagar
	name = "Azura"
	dynasty = dynasty_ruhagar
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_azura_ruhagar
	father = ruhagar0003
	female = yes
	disallow_random_traits = yes

	trait = race_human
 	trait = curious

	1007.3.5 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#	}
	#}
}

karyad0001 = { #Nuriye Karyad
	name = "Nuriye"
	dynasty = dynasty_karyad
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_nuriye_karyad
	#father = 
	#mother = 
	female = yes
	disallow_random_traits = yes

	trait = race_human
 	trait = education_stewardship_2

	966.12.3 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	1020.3.16 = {
		death = yes
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#	}
	#}
}

karyad0002 = { #Arwia Karyad
	name = "Arwia"
	dynasty = dynasty_karyad
	religion = "way_of_kings"
	culture = "akalsesi"
	dna = dna_arwia_karyad
	#father = 
	mother = karyad0001
	female = yes
	disallow_random_traits = yes

	diplomacy = 4
	martial = 4
	stewardship = 8
	intrigue = 6
	learning = 7
	prowess = 8

	trait = race_human
	trait = education_martial_3
 	trait = patient
	trait = sadistic
	trait = arrogant
	trait = physique_good_2
	trait = shieldmaiden
	trait = unyielding_defender

	974.5.13 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			add_character_flag = has_scripted_appearance
		}
	}

	997.3.12 = {
		add_matrilineal_spouse = s_ama0006
	}

	1001.1.1 = {
		give_nickname = nick_nanarta
	}

	portrait_override ={
		portrait_modifier_overrides={
			custom_beards=female_empty
			custom_hair=f_hair_fp3_iranian_03
			custom_clothes=f_clothes_sec_fp3_iranian_nob_02_lo
			custom_headgear=f_headgear_sec_fp3_iranian_nob_01
			custom_legwear=female_legwear_secular_mena_nobility_01
		}
	}
}

karyad0003 = { #Kamelya Karyad
	name = "Kamelya"
	dynasty = dynasty_karyad
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_kamelya_karyad
	father = s_ama0006
	mother = karyad0002
	female = yes
	disallow_random_traits = yes

	diplomacy = 5
	martial = 8
	stewardship = 6
	intrigue = 3
	learning = 4
	prowess = 5

	trait = race_human
	trait = education_martial_2
 	trait = impatient
	trait = ambitious
	trait = vengeful
	trait = lifestyle_hunter
	trait = rough_terrain_expert
	trait = shieldmaiden

	998.4.12 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#add_character_flag = has_scripted_appearance
		}
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_beards=
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#		custom_legwear=
	#	}
	#}
}

karyad0004 = { #Elina Karyad
	name = "Elina"
	dynasty = dynasty_karyad
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_elina_karyad
	father = s_ama0006
	mother = karyad0002
	female = yes
	disallow_random_traits = yes

	diplomacy = 12
	martial = 4
	stewardship = 17
	intrigue = 4
	learning = 10
	prowess = 9

	trait = race_human
	trait = education_stewardship_3
 	trait = greedy
	trait = stubborn
	trait = diligent
	trait = physique_good_1

	1000.5.9 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#add_character_flag = has_scripted_appearance
		}
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_beards=
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#		custom_legwear=
	#	}
	#}
}

karyad0005 = { #Sargin Karyad
	name = "Sargin"
	dynasty = dynasty_karyad
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_sargin_karyad
	#father = 
	mother = karyad0002
	disallow_random_traits = yes

	trait = race_human
	trait = bossy

	1007.4.12 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#add_character_flag = has_scripted_appearance
		}
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_beards=
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#		custom_legwear=
	#	}
	#}
}

karyad0006 = { #Fehan Karyad
	name = "Fehan"
	dynasty = dynasty_karyad
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_fehan_karyad
	#father = 
	mother = karyad0001
	disallow_random_traits = yes

	trait = race_human
	trait = education_learning_1
	trait = lazy
	trait = patient
	trait = forgiving
	trait = lifestyle_gardener

	979.12.5 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#add_character_flag = has_scripted_appearance
		}
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_beards=
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#		custom_legwear=
	#	}
	#}
}

karyad0007 = { #Naram Karyad
	name = "Naram"
	dynasty = dynasty_karyad
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_naram_karyad
	father = karyad0006
	#mother = 
	disallow_random_traits = yes

	trait = race_human
	trait = education_diplomacy_4
	trait = lustful
	trait = gregarious
	trait = trusting
	trait = poet
	trait = beauty_good_1

	999.11.17 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#add_character_flag = has_scripted_appearance
		}
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_beards=
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#		custom_legwear=
	#	}
	#}
}

s_ama0001 = { #Fereydun Šama
	name = "Fereydun"
	dynasty = dynasty_s_ama
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_fereydun_s_ama
	#father = 
	#mother = 
	disallow_random_traits = yes

	trait = race_human
	trait = education_stewardship_1

	955.6.21 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#add_character_flag = has_scripted_appearance
		}
	}

	1018.3.21 = {
		death = yes
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_beards=
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#		custom_legwear=
	#	}
	#}
}

s_ama0002 = { #Azariel Šama
	name = "Azariel"
	dynasty = dynasty_s_ama
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_azariel_s_ama
	father = s_ama0001
	#mother = 
	disallow_random_traits = yes

	trait = race_human
	trait = education_learning_2
	trait = diligent
	trait = shy
	trait = zealous
	trait = scholar

	diplomacy = 12
	martial = 7
	stewardship = 8
	intrigue = 5
	learning = 16
	prowess = 7

	983.5.3 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#add_character_flag = has_scripted_appearance
		}
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_beards=
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#		custom_legwear=
	#	}
	#}
}

s_ama0003 = { #Rimuš Šama
	name = "Rimus_"
	dynasty = dynasty_s_ama
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_rimus__s_ama
	father = s_ama0001
	#mother = 
	disallow_random_traits = yes

	trait = race_human
	trait = education_martial_2

	981.12.4 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#add_character_flag = has_scripted_appearance
		}
	}

	1017.3.15 = {
		death = yes
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_beards=
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#		custom_legwear=
	#	}
	#}
}

s_ama0004 = { #Aylara Šama
	name = "Aylara"
	dynasty = dynasty_s_ama
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_aylara_s_ama
	father = s_ama0001
	#mother = 
	female = yes
	disallow_random_traits = yes

	trait = race_human
	trait = education_intrigue_4
	trait = deceitful
	trait = chaste
	trait = vengeful
	trait = hashishiyah

	diplomacy = 3
	martial = 5
	stewardship = 4
	intrigue = 19
	learning = 5
	prowess = 9

	994.6.17 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#add_character_flag = has_scripted_appearance
		}
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_beards=
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#		custom_legwear=
	#	}
	#}
}

s_ama0005 = { #Farduš Šama
	name = "Fardus_"
	dynasty = dynasty_s_ama
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_fardus__s_ama
	father = s_ama0001
	#mother = 
	disallow_random_traits = yes

	trait = race_human
	trait = education_stewardship_3
	trait = gluttonous
	trait = diligent
	trait = stubborn
	trait = architect

	diplomacy = 7
	martial = 5
	stewardship = 9
	intrigue = 5
	learning = 15
	prowess = 3

	997.2.28 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#add_character_flag = has_scripted_appearance
		}
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_beards=
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#		custom_legwear=
	#	}
	#}
}

s_ama0006 = { #Fereydun Šama
	name = "Fereydun"
	dynasty = dynasty_s_ama
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_fereydun_2_s_ama
	father = s_ama0001
	#mother = 
	disallow_random_traits = yes

	trait = race_human
	trait = education_learning_2
	trait = content
	trait = eccentric
	trait = generous
	trait = lifestyle_gardener

	976.5.12 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#add_character_flag = has_scripted_appearance
		}
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_beards=
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#		custom_legwear=
	#	}
	#}
}

taumakar0001 = { #Tigran Taumakar
	name = "Tigran"
	dynasty = dynasty_taumakar
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_tigran_taumakar
	#father =
	#mother = 
	disallow_random_traits = yes

	trait = race_human
	trait = education_martial_2

	945.5.18 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#add_character_flag = has_scripted_appearance
		}
	}

	993.12.9 = {
		death = yes
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_beards=
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#		custom_legwear=
	#	}
	#}
}

taumakar0002 = { #Asiah Taumakar
	name = "Asiah"
	dynasty = dynasty_taumakar
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_asiah_taumakar
	father = taumakar0001 #Tigran Taumakar
	#mother = 
	female = yes
	disallow_random_traits = yes

	trait = race_human
	trait = education_stewardship_2
	trait = lifestyle_gardener
	trait = vengeful
	trait = ambitious
	trait = greedy

	diplomacy = 5
	martial = 4
	stewardship = 9
	intrigue = 2
	learning = 8
	prowess = 3

	964.3.15 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#add_character_flag = has_scripted_appearance
		}
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_beards=
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#		custom_legwear=
	#	}
	#}
}

taumakar0003 = { #Takhmasp Taumakar
	name = "Takhmasp"
	dynasty = dynasty_taumakar
	religion = "way_of_kings"
	culture = "akalsesi"
	dna = dna_takhmasp_taumakar
	father = taumakar0001 #Tigran Taumakar
	#mother = 
	disallow_random_traits = yes

	trait = race_human
	trait = education_diplomacy_4
	trait = diplomat
	trait = patient
	trait = honest
	trait = forgiving
	trait = intellect_good_1

	diplomacy = 2
	martial = 6
	stewardship = 7
	intrigue = 7
	learning = 5
	prowess = 5

	966.1.30 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#add_character_flag = has_scripted_appearance
		}
	}

	portrait_override ={
		portrait_modifier_overrides={
			custom_beards=m_beard_fp3_iranian_02
			custom_hair=male_hair_fp2_iberian_muslim_02
			custom_clothes=male_clothes_secular_dde_abbasid_nobility_01_low
			custom_headgear=male_headgear_secular_mena_high_nobility_01
			custom_legwear=male_legwear_secular_mena_nobility_01
		}
	}
}

taumakar0004 = { #Xerak Taumakar
	name = "Xerak"
	dynasty = dynasty_taumakar
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_xerak_taumakar
	father = taumakar0001 #Tigran Taumakar
	#mother = 
	disallow_random_traits = yes

	trait = race_human
	trait = education_stewardship_1
	trait = gluttonous
	trait = lazy
	trait = honest
	trait = beauty_good_2

	diplomacy = 4
	martial = 6
	stewardship = 6
	intrigue = 13
	learning = 8
	prowess = 4

	971.3.26 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#add_character_flag = has_scripted_appearance
		}
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_beards=
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#		custom_legwear=
	#	}
	#}
}

taumakar0005 = { #Derya Taumakar
	name = "Derya"
	dynasty = dynasty_taumakar
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_derya_taumakar
	father = taumakar0003 #Takhmasp Taumakar
	#mother = 
	female = yes
	disallow_random_traits = yes

	trait = race_human
	trait = education_martial_3
	trait = diligent
	trait = content
	trait = temperate
	trait = organizer

	diplomacy = 6
	martial = 7
	stewardship = 3
	intrigue = 9
	learning = 2
	prowess = 8

	989.5.14 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#add_character_flag = has_scripted_appearance
		}
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_beards=
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#		custom_legwear=
	#	}
	#}
}

taumakar0006 = { #Zimudar Taumakar
	name = "Zimudar"
	dynasty = dynasty_taumakar
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_zimudar_taumakar
	father = taumakar0004 #Xerak Taumakar
	#mother = 
	disallow_random_traits = yes

	trait = race_human
	trait = education_intrigue_3
	trait = lustful
	trait = arbitrary
	trait = trusting
	trait = shrewd

	diplomacy = 4
	martial = 6
	stewardship = 5
	intrigue = 6
	learning = 4
	prowess = 7

	996.8.12 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#add_character_flag = has_scripted_appearance
		}
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_beards=
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#		custom_legwear=
	#	}
	#}
}

taumakar0007 = { #Darian Taumakar
	name = "Darian"
	dynasty = dynasty_taumakar
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna_darian_taumakar
	#father = 
	mother = taumakar0005 #Derya Taumakar
	disallow_random_traits = yes

	trait = race_human
	trait = pensive

	1007.12.24 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#add_character_flag = has_scripted_appearance
		}
	}

	#portrait_override ={
	#	portrait_modifier_overrides={
	#		custom_beards=
	#		custom_hair=
	#		custom_clothes=
	#		custom_headgear=
	#		custom_legwear=
	#	}
	#}
}

farasyana0001 = { # Mithraš Farasyana
	name = "Mithras_"
	dynasty = dynasty_farasyana
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__farasyana
	disallow_random_traits = yes

	trait = race_human
	trait = just
	trait = honest
	trait = forgiving

	962.10.13 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}

	1013.2.5 = {
		death = yes
	}
}

farasyana0002 = { # Marzan Farasyana
	name = "Marzan"
	dynasty = dynasty_farasyana
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__farasyana
	disallow_random_traits = yes
	father = farasyana0001

	trait = race_human
	trait = education_learning_2
	trait = gregarious
	trait = fickle
	trait = just
	trait = pilgrim

	diplomacy = 8
	martial = 7
	stewardship = 9
	intrigue = 6
	learning = 7
	prowess = 11

	983.3.1 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}
}

farasyana0003 = { # Barzas Farasyana
	name = "Barzas"
	dynasty = dynasty_farasyana
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__farasyana
	disallow_random_traits = yes
	father = farasyana0001

	trait = race_human
	trait = education_martial_3
	trait = zealous
	trait = impatient
	trait = honest
	trait = scarred
	trait = physique_good_1

	986.5.17 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}
}

farasyana0004 = { # Nylara Farasyana
	name = "Nylara"
	dynasty = dynasty_farasyana
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__farasyana
	disallow_random_traits = yes
	father = farasyana0001
	female = yes

	trait = race_human
	trait = education_stewardship_1
	trait = gregarious
	trait = diligent
	trait = forgiving
	trait = lifestyle_physician

	986.12.4 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}
}

farasyana0005 = { # Ardan Farasyana
	name = "Ardan"
	dynasty = dynasty_farasyana
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__farasyana
	disallow_random_traits = yes
	father = farasyana0002

	trait = race_human
	trait = education_stewardship_3
	trait = trusting
	trait = arbitrary
	trait = humble
	trait = twin
	trait = physique_good_1

	diplomacy = 9
	martial = 8
	stewardship = 5
	intrigue = 8
	learning = 5
	prowess = 7

	1002.6.12 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}
}

farasyana0006 = { # Ardana Farasyana
	name = "Ardana"
	dynasty = dynasty_farasyana
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__farasyana
	disallow_random_traits = yes
	father = farasyana0002
	female = yes

	trait = race_human
	trait = education_diplomacy_3
	trait = trusting
	trait = arbitrary
	trait = arrogant
	trait = twin
	trait = physique_good_1

	diplomacy = 3
	martial = 6
	stewardship = 11
	intrigue = 7
	learning = 8
	prowess = 7

	1002.6.12 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}
}

farasyana0007 = { # Idris Farasyana
	name = "Idris"
	dynasty = dynasty_farasyana
	religion = "way_of_kings"
	culture = "akalsesi"
	#dna = dna__farasyana
	disallow_random_traits = yes
	mother = farasyana0004

	trait = race_human
	trait = education_intrigue_3
	trait = generous
	trait = lazy
	trait = gregarious
	trait = poet
	trait = strong

	1006.8.9 = {
		birth = yes
		effect = {
			set_variable = no_purist_trait
			#	add_character_flag = has_scripted_appearance
		}
	}
}