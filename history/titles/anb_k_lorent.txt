
k_lorent = {
	1000.1.1 = { change_development_level = 8 }
	
	899.12.31 = {
		insert_title_history = k_redwood # continue using redwood regnal numbers
	}
	900.1.1 = { # Foundation of the Kingdom of Lorent
		# holder = lorentis_0001 # Lorevarn II Lorentis
	}
	947.1.9 = {
		holder = lorentis_0003 # Rewan VII Lorentis
		succession_laws = { equal_law } # was male only
	}
	954.11.16 = {
		holder = lorentis_0005 # Reanna I Lorentis
	}
	975.2.28 = {
		holder = lorentis_0007 # Reanna II Lorentis
	}
	990.9.15 = {
		holder = lorentis_0011 # Rewan VIII "the Thorn of the West" Lorentis
	}
	1014.1.4 = {
		holder = lorentis_0013 # Kylian Lorentis
	}
	1015.11.1 = {
		holder = lorentis_0014 # Ruben Lorentis
	}
	1020.10.27 = {
		holder = 2 # Rean Siloriel
	}
}

d_ainethan = {
	1000.1.1 = { change_development_level = 10 }
	947.1.9 = {
		liege = k_lorent
		holder = aldegarde_0016 # Guillame Aldegarde
	}
	961.11.29 = {
		holder = aldegarde_0019 # Athara Aldegarde
	}
	997.11.15 = {
		holder = aldegarde_0020 # Roger Aldegarde
	}
	1019.03.10 = {
		holder = aldegarde_0021 # Adelinde Aldegarde
	}
}

c_ainethan = {
	525.12.25 = { # Formation of the High Kingdom
		liege = k_redfort
	}
	552.7.19 = {
		holder = aldegarde_0001 # Ruben Aldegarde
	}
	602.1.15 = {
		holder = aldegarde_0002 # Ricart I Aldegarde
	}
	622.10.3 = { # War of the Three Roses
		liege = k_lord_regent_of_redfort
	}
	639.8.21 = {
		holder = aldegarde_0004 # Lorens Aldegarde
	}
	676.2.18 = {
		holder = aldegarde_0005 # Ricart II Aldegarde
	}
	712.12.7 = {
		holder = aldegarde_0006 # Mauric Aldegarde
	}
	740.2.13 = {
		holder = aldegarde_0007 # Riol I Aldegarde
	}
	760.7.16 = {
		holder = aldegarde_0009 # Ricart III "the Late" Aldegarde
	}
	810.4.8 = {
		holder = aldegarde_0010 # Morgan I Aldegarde
	}
	830.4.23 = {
		holder = aldegarde_0011 # Riol II Aldegarde
	}
	842.8.8 = {
		holder = aldegarde_0012 # Morgan II Aldegarde
	}
	868.3.4 = {
		holder = aldegarde_0014 # Geraint Aldegarde
	}
	869.2.26 = { # Derannic Conquest of Lencenor
		liege = k_deranne
	}
	885.1.1 = { # Carneter occupy Redfort
		liege = k_redfort
	}
	889.1.22 = {
		holder = aldegarde_0016 # Guillame Aldegarde
	}
	900.1.1 = { # Formation of Lorent
		liege = k_lorent
	}
	961.11.29 = {
		holder = aldegarde_0019 # Athara Aldegarde
	}
	997.11.15 = {
		holder = aldegarde_0020 # Roger Aldegarde
	}
	1019.03.10 = {
		holder = aldegarde_0021 # Adelinde Aldegarde
	}
}

c_oldport = {
	1000.1.1 = { change_development_level = 11 }
	
	888.11.27 = {
		holder = oldport_0001
	}
	900.1.1 = {
		liege = k_lorent
	}
	938.4.6 = {
		holder = oldport_0002
	}
	947.1.9 = {
		liege = d_ainethan
	}
	976.1.2 = {
		holder = lorentis_0007 # Reanna II Lorentis
	}
	990.9.15 = {
		holder = lorentis_0011 # Rewan VIII "the Thorn of the West" Lorentis
	}
	1014.1.4 = {
		holder = lorentis_0013 # Kylian Lorentis
	}
	1015.11.1 = {
		holder = lorentis_0014 # Ruben Lorentis
	}
	1020.10.27 = {
		holder = 2 # Rean Siloriel
	}
}

c_casthil = {
	1000.1.1 = { change_development_level = 10 }
	900.1.1 = {
		liege = k_lorent
	}
	947.1.9 = {
		liege = d_ainethan
	}
	1002.12.1 = {
		holder = 163	#Jacques father Lorens
	}
}

c_lorentaine = {
	1000.1.1 = { change_development_level = 22 }
}

# b_highcour = {
# 	1010.2.7 = {
# 		holder = lorentish_0001
# 	}
# }

c_rewanfork = {
	1000.1.1 = { change_development_level = 10 }
	489.3.16 = {
		holder = harascilde_0002 # Rewan of Harascilde
	}
	515.7.19 = {
		holder = harascilde_0003 # Fracan I of Harascilde
	}
	525.12.25 = { # Formation of the High Kingdom
		liege = k_redfort
	}
	556.3.7 = {
		holder = harascilde_0005 # Cadarvan of Harascilde
	}
	587.11.10 = {
		holder = harascilde_0006 # Fracan II of Harascilde
	}
	604.7.4 = {
		holder = harascilde_0008 # Caradan I of Harascilde
	}
	622.10.3 = { # War of the Three Roses
		liege = k_lord_regent_of_redfort
	}
	668.11.13 = {
		holder = harascilde_0009 # Hesdren of Harascilde
	}
	670.10.9 = {
		holder = harascilde_0010 # Caradan II of Harascilde
	}
	712.9.12 = {
		holder = harascilde_0012 # Florien of Harascilde,
	}
	720.9.5 = {
		holder = harascilde_0013 # Artan I "the Falcon" of Harascilde
	}
	754.5.14 = {
		holder = harascilde_0014 # Lorens of Harascilde
	}
	789.10.12 = {
		holder = harascilde_0015 # Ruben "the Handsome" of Harascilde
	}
	810.6.8 = {
		holder = harascilde_0017 # Artan II of Harascilde
	}
	855.12.13 = {
		holder = harascilde_0018 # Caradan III "the Brave" of Harascilde
	}
	869.2.25 = {
		holder = harascilde_0020 # Artan III of Harascilde
	}
	869.2.26 = { # Derannic Conquest of Lencenor
		liege = k_deranne
	}
	879.5.13 = {
		holder = harascilde_0021 # Adrien I of Harascilde
	}
	885.1.1 = { # Carneter occupy Redfort
		liege = k_redfort
	}
	900.1.1 = { # Formation of Lorent
		liege = k_lorent
	}
	936.6.30 = {
		holder = harascilde_0022 # Rewan II of Harascilde
	}
	947.6.12 = {
		holder = harascilde_0024 # Adrien II of Harascilde
	}
	979.5.14 = {
		holder = harascilde_0025 # Artan IV of Harascilde
	}
	996.8.14 = {
		holder = harascilde_0026 # Caradan IV "the Shield of the West" of Harascilde
	}
}

c_redfort = {
	1000.1.1 = { change_development_level = 9 }
	869.2.25 = {
		holder = harascilde_0020 # Artan III of Harascilde
	}
	869.2.26 = { # Derannic Conquest of Lencenor
		liege = k_deranne
	}
	879.5.13 = {
		holder = harascilde_0021 # Adrien I of Harascilde
	}
	885.1.1 = { # Carneter occupy Redfort
		liege = k_redfort
	}
	900.1.1 = { # Formation of Lorent
		liege = k_lorent
	}
	936.6.30 = {
		holder = harascilde_0022 # Rewan II of Harascilde
	}
	947.6.12 = {
		holder = harascilde_0024 # Adrien II of Harascilde
	}
	979.5.14 = {
		holder = harascilde_0025 # Artan IV of Harascilde
	}
	996.8.14 = {
		holder = harascilde_0026 # Caradan IV "the Shield of the West" of Harascilde
	}
}

b_harascilde = {
	427.6.1 = {
		holder = harascilde_0001 # Breselgar of Harascilde # his birthdate, why ? cause placeholder
	}
	489.3.16 = {
		holder = harascilde_0002 # Rewan of Harascilde
	}
	515.7.19 = {
		holder = harascilde_0003 # Fracan I of Harascilde
	}
	556.3.7 = {
		holder = harascilde_0005 # Cadarvan of Harascilde
	}
	587.11.10 = {
		holder = harascilde_0006 # Fracan II of Harascilde
	}
	604.7.4 = {
		holder = harascilde_0008 # Caradan I of Harascilde
	}
	668.11.13 = {
		holder = harascilde_0009 # Hesdren of Harascilde
	}
	670.10.9 = {
		holder = harascilde_0010 # Caradan II of Harascilde
	}
	712.9.12 = {
		holder = harascilde_0012 # Florien of Harascilde,
	}
	720.9.5 = {
		holder = harascilde_0013 # Artan I "the Falcon" of Harascilde
	}
	754.5.14 = {
		holder = harascilde_0014 # Lorens of Harascilde
	}
	789.10.12 = {
		holder = harascilde_0015 # Ruben "the Handsome" of Harascilde
	}
	810.6.8 = {
		holder = harascilde_0017 # Artan II of Harascilde
	}
	855.12.13 = {
		holder = harascilde_0018 # Caradan III "the Brave" of Harascilde
	}
	869.2.25 = {
		holder = harascilde_0020 # Artan III of Harascilde
	}
	879.5.13 = {
		holder = harascilde_0021 # Adrien I of Harascilde
	}
	936.6.30 = {
		holder = harascilde_0022 # Rewan II of Harascilde
	}
	947.6.12 = {
		holder = harascilde_0024 # Adrien II of Harascilde
	}
	979.5.14 = {
		holder = harascilde_0025 # Artan IV of Harascilde
	}
	996.8.14 = {
		holder = harascilde_0026 # Caradan IV "the Shield of the West" of Harascilde
	}
}

c_lorenith = {
	1000.1.1 = { change_development_level = 9 }
	
	598.8.10 = {
		holder = crownscour_0001 # Riol I of Crownscour
	}
	655.10.16 = {
		holder = crownscour_0003 # Florien of Crownscour
	}
	697.2.2 = {
		holder = crownscour_0005 # Riol II of Crownscour
	}
	# 723.2.25 = {
		# holder = 0
	# }
	
	1020.10.27 = {
		holder = 2 # Rean Siloriel
	}
}

c_ionnidar = {
	800.1.1 = {
		liege = k_lorent
	}
	948.2.16 = { # this date make no sense, dude wasn't born yet
		holder = 707
	}
	1000.1.1 = {
		liege = k_lorent
	}
}

d_rosefield = {
	869.1.2 = {
		holder = caradantis_0001 # Caradan I Caradantis
		liege = k_redwood
	}
	900.1.1 = {
		liege = k_lorent
	}
	905.2.12 = {
		holder = caradantis_0002 # Lorenan I Caradantis
	}
	919.1.12 = {
		holder = caradantis_0004 # Maren I Caradantis
	}
	949.12.9 = {
		holder = caradantis_0006 # Emmeran I Caradantis
	}
	969.3.12 = {
		holder = caradantis_0009 # Lorenan II Caradantis
	}
	1000.2.12 = {
		holder = caradantis_0015 # Maren II Caradantis
	}
	1001.5.17 = {
		holder = 0
	}
	1021.10.31 = { # Grand Ball of Anbenncost + Treaty of Anbenncost
		holder = lasean_0001 # Korvin "Rose Devil" Sil Lasean
	}
}

c_lasean = {
	869.1.2 = {
		holder = caradantis_0001 # Caradan I Caradantis
	}
	905.2.12 = {
		holder = caradantis_0002 # Lorenan I Caradantis
	}
	919.1.12 = {
		holder = caradantis_0004 # Maren I Caradantis
	}
	949.12.9 = {
		holder = caradantis_0006 # Emmeran I Caradantis
	}
	969.3.12 = {
		holder = caradantis_0009 # Lorenan II Caradantis
	}
	1000.2.12 = {
		holder = caradantis_0015 # Maren II Caradantis
	}
	1001.5.17 = {
		holder = lasean_0001 # Korvin "Rose Devil" Sil Lasean
	}
}

c_ar_esta = {
	869.1.2 = {
		holder = caradantis_0001 # Caradan I Caradantis
	}
	905.2.12 = {
		holder = caradantis_0002 # Lorenan I Caradantis
	}
	919.1.12 = {
		holder = caradantis_0004 # Maren I Caradantis
	}
	949.12.9 = {
		holder = caradantis_0006 # Emmeran I Caradantis
	}
	969.3.12 = {
		holder = caradantis_0009 # Lorenan II Caradantis
	}
	1000.2.12 = {
		holder = caradantis_0015 # Maren II Caradantis
	}
	1001.5.17 = {
		holder = lasean_0001 # Korvin "Rose Devil" Sil Lasean
	}
}

c_rosefield = {
	900.1.1 = {
		liege = k_lorent
	}
	1021.10.31 = {
		holder = 708
		liege = d_rosefield
	}
}

d_redglades = {
	1000.1.1 = { change_development_level = 7 }
	1000.1.1 = {
		liege = k_lorent
		succession_laws = { elven_elective_succession_law elf_only  }
	}
	1018.1.1 = {
		holder = 15	#Ioriel
	}
}

c_ioriellen = {
	1000.1.1 = { change_development_level = 10 }
}

d_rewanwood = {
	543.1.1 = {
		liege = k_redwood
		holder = rewantis_0001 # Rewan I "Ironbark" Rewantis
	}
	582.12.2 = {
		holder = rewantis_0004 # Maren I Rewantis
	}
	623.9.28 = {
		holder = rewantis_0005 # Rewan II Rewantis
	}
	641.3.31 = {
		holder = rewantis_0006 # Loren I Rewantis
	}
	665.2.10 = {
		holder = rewantis_0007 # Maren II Rewantis
	}
	670.9.18 = {
		holder = rewantis_0008 # Rewan III Rewantis
	}
	695.1.3 = {
		holder = rewantis_0009 # Maren III "the Poet" Rewantis
	}
	724.10.10 = {
		holder = rewantis_0010 # Rewan IV "the Grey" Rewantis
	}
	770.9.8 = {
		holder = rewantis_0012 # Ruben I Rewantis
	}
	801.11.2 = {
		holder = rewantis_0014 # Rewan V "the White" Rewantis
	}
	814.10.5 = {
		holder = rewantis_0015 # Sorewan I Rewantis
	}
	844.10.16 = {
		holder = rewantis_0017 # Rewan VI Rewantis
	}
	860.4.9 = {
		holder = rewantis_0018 # Lorevarn I Rewantis
	}
	879.5.10 = {
		holder = lorentis_0001 # Lorevarn II Lorentis
	}
	900.1.1 = {
		liege = k_lorent
		holder = rewantis_0019 # Loren II Rewantis
	}
	937.4.11 = {
		holder = rewantis_0020 # Rewan VII Rewantis
	}
	957.11.6 = {
		holder = rewantis_0022 # Loren III Rewantis
	}
	990.10.26 = {
		holder = rewantis_0023 # Arthel Rewantis
	}
	1014.2.6 = {
		holder = rewantis_0025 # Jaspar Rewantis
	}
}

c_autumnglade = {
	1000.1.1 = {
		liege = "d_redglades"
	}
	1018.1.1 = {
		holder = 535	#Delsaran Gladeguard
	}
}

c_wesmar = {
	1000.1.1 = { change_development_level = 10 }
}

d_lower_bloodwine = {
	1000.1.1 = { change_development_level = 11 }
	1000.1.1 = {
		liege = k_lorent
		holder = 20000	#Thayen
	}
}

c_lower_bloodwine = {
	1000.1.1 = {
		holder = 20000	#Thayen
	}
}

c_pircost = {
	1000.1.1 = {
		holder = 20000	#Thayen
	}
}

c_foxalley = {
	950.1.1 = {
		liege = d_enteben
	}
	988.9.25 = {
		holder = 582	#Roen the Elder Arthelis
	}
	1014.7.14 = {
		holder = 583	#Roen the Younger Arthelis. His father dies in Battle of Morban Flats
	}
}

c_minar = {
	1000.1.1 = { change_development_level = 14 }
	1000.1.1 = {
		holder = 170 # Minaran Archpriestess
	}
}

c_wineport = {
	1000.1.1 = { change_development_level = 16 }
	1020.01.01 = {
		liege = k_lorent
		holder = 180 # Runan
		government = republic_government
	}
}

d_upper_bloodwine = {
	1000.1.1 = { change_development_level = 11 }
	833.7.2 = {
		liege = k_enteben
		holder = caylenoris_0001
	}
	868.8.6 = {
		holder = caylenoris_0002
	}
	893.8.6 = {
		holder = caylenoris_0003
	}
	900.1.1 = {
		liege = k_lorent
	}
	917.8.6 = {
		holder = caylenoris_0006
	}
	973.8.6 = {
		holder = caylenoris_0007
	}
	985.8.6 = {
		holder = caylenoris_0008
	}
	1020.1.2 = {
		holder = kyliande_0001 # Morganne sil Kyliande
	}
}

c_kyliande = {
	1000.1.1 = { change_development_level = 13 }
	1020.1.2 = {
		holder = kyliande_0001 # Morganne sil Kyliande
	}
}

c_nurionn = {
	1020.1.2 = {
		holder = kyliande_0001 # Morganne sil Kyliande
	}
}

c_palevine = {
	1000.1.1 = { change_development_level = 12 }
	969.4.15 = {
		liege = k_lorent
		holder = 705	#Beltram Ventis
	}
	1010.6.21 = {
		liege = k_lorent
		holder = 598	#Andred Ventis
	}
	1020.1.2 = {
		liege = k_lorent
		holder = 554	#Beltram Ventis
	}
}

c_greenfield = {
	1000.1.1 = { change_development_level = 11 }
	969.4.15 = {
		liege = k_lorent
		holder = 705	#Beltram Ventis
	}
	1010.6.21 = {
		liege = k_lorent
		holder = 598	#Andred Ventis
	}
	1020.1.2 = {
		liege = k_lorent
		holder = 554	#Beltram Ventis
	}
}
