k_balmire = {
	1000.1.1 = { change_development_level = 6 }
	1018.7.18 = {
		holder = 93 #Albert Balmire
	}
}

c_bal_mire = {
	1000.1.1 = { change_development_level = 7 }
}

d_falsemire = {
	1000.1.1 = { change_development_level = 8 }
}

c_falseharbour = {
	1000.1.1 = { change_development_level = 10 }
}

c_badeben = {
	1000.1.1 = { change_development_level = 9 }
}

#Old Agrador titles
d_agradalen = {
	1000.1.1 = { change_development_level = 8 }
}

c_agradord = {
	1000.1.1 = { change_development_level = 9 }
	1021.10.31 = {
		holder = 60013
		liege = d_acengard
	}
}

c_uelced = {
	1000.1.1 = { change_development_level = 9 }
	1021.10.31 = {
		holder = 60013
		liege = d_acengard
	}
}

c_athfork = {
	1000.1.1 = { change_development_level = 10 }
	1021.10.31 = {
		holder = 60011
		liege = d_acengard
	}
}

c_ridgehyl = {
	1000.1.1 = { change_development_level = 10 }
	1021.10.31 = {
		holder = 60011
		liege = d_acengard
	}
}


d_cannwood = {
	1000.1.1 = { change_development_level = 8 }
	1021.10.31 = {
		holder = 60014
	}
}

c_cannwood = {
	1000.1.1 = { change_development_level = 7 }
	1021.10.31 = {
		holder = 60015
		liege = d_acengard
	}
}

c_forksgrove = {
	1000.1.1 = { change_development_level = 7 }
	1021.10.31 = {
		holder = 60012
		liege = d_acengard
	}
}

c_westwatch = {
	1000.1.1 = { change_development_level = 9 }
}

c_acengard = {
	1021.10.31 = {
		holder = 60014
	}
}

c_cannwic = {
	1021.10.31 = {
		holder = 60015
		liege = d_acengard
	}
}

c_esckerfort = {
	1021.10.31 = {
		holder = 60014
	}
}