d_baganas = {
	1000.1.1 = { change_development_level = 4 }

	980.7.1 = {
		holder = adad_0001
	}
    1021.4.5 = {
		holder = adad_0002
	}
}

c_baganas = {
	1021.4.5 = {
		holder = adad_0002
	}
}

c_ebbusubtu = {
	1000.1.1 = { change_development_level = 5 }

	992.2.6 = {
		holder = zaid_0002
	}
}

c_nasratrub = {
	1000.1.1 = { change_development_level = 4 }

	1000.1.2 = {
		holder = rijascar_0001
	}
}

c_harnio = {
	1000.1.1 = { change_development_level = 4 }

	1000.1.2 = {
		holder = bulati_0001
	}
}

c_ianvireri = {
	1000.1.1 = { change_development_level = 7 }
}

c_tarnuthen = {
	1000.1.1 = { change_development_level = 7 }
}

c_chorfo = {
	1000.1.1 = { change_development_level = 7 }
}

c_nuzidastu = {
	1000.1.1 = { change_development_level = 7 }
}

c_siadunaiun = {
	1000.1.1 = { change_development_level = 12 }
}

c_kotidurnor = {
	1000.1.1 = { change_development_level = 6 }
}

c_mermigan = {
	1000.1.1 = { change_development_level = 9 }
}

c_istartovo = {
	1000.1.1 = { change_development_level = 7 }
}

c_siphefireri = {
	1000.1.1 = { change_development_level = 8 }
}

c_larfira = {
	1000.1.1 = { change_development_level = 11 }
}

c_trasenarbu = {
	1000.1.1 = { change_development_level = 6 }
}

c_shibahurran = {
	1000.1.1 = { change_development_level = 7 }
}

c_garanias = {
	1000.1.1 = { change_development_level = 6 }
}