k_rimurhals = {
	1000.1.1 = { change_development_level = 4 }
}

c_redgarhavn = {
	1000.1.1 = { change_development_level = 8 }
	
	1003 = {
		holder = 300012
		government = tribal_government
	}
}

c_skaldol = {
	1000.1.1 = { change_development_level = 6 }
	
	1003 = {
		holder = 300012
		government = theocracy_government
	}
}

c_rosstavik = {
	1003 = {
		holder = 300012
		government = tribal_government
	}
}

b_fjorhavn = {
	1022 = {
		holder =  fjorhavn_0005
		government = tribal_government
	}
}

c_jotunhamr = {
	1003 = {
		holder = 300012
		government = theocracy_government
	}
}

d_drekkiskali = {
	992 = {
		holder = 300013
		government = tribal_government
	}
}

c_olavsborg = {
	981 = {
		holder = 300013
		government = tribal_government
	}
}

d_esfjall = {
	1019 = {
		holder = 300014
		government = tribal_government
	}
}

c_naugsvol = {
	1005 = {
		holder = 300015
		government = tribal_government
	}
}

c_baldersby = {
	962 = {
		holder = 300016
		government = tribal_government
	}
}

c_silfrborg = {
	985 = {
		holder = 300017
		government = tribal_government
	}
}

d_avnkaup = {
	1020 = {
		holder = 300018
		government = tribal_government
	}
}