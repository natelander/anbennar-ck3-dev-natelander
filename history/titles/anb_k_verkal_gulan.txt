﻿
k_verkal_gulan = {
	1000.1.1 = { change_development_level = 3 }
	903.4.17 = {
		holder = 128 # Girud of Verkal Gulan
	}
	921.9.24 = {
		liege = e_middle_dwarovar
	}
}

d_verkal_gulan = {
	903.4.17 = {
		holder = 128 # Girud of Verkal Gulan
	}
}

c_verkal_gulan = {
	903.4.17 = {
		holder = 128 # Girud of Verkal Gulan
	}
	1000.1.1 = { change_development_level = 20 }
}

c_heros_vale = {
	903.4.17 = {
		holder = 128 # Girud of Verkal Gulan
	}
	1000.1.1 = { change_development_level = 13 }
}
